package com.example.informatque.education.viewControllers;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.informatque.education.R;
import com.example.informatque.education.helpers.ParentActivity;
import com.example.informatque.education.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CourseDetailsViewController extends ParentActivity {

    //regionBindViews
    @BindView(R.id.tv_CourseCodeValue)
    TextView tv_CourseCodeValue;

    @BindView(R.id.tv_CourseNameValue)
    TextView tv_CourseNameValue;

    @BindView(R.id.tv_CourseCreditHourValue)
    TextView tv_CourseCreditHourValue;

    @BindView(R.id.tv_CourseGradeValue)
    TextView tv_CourseGradeValue;

    @BindView(R.id.tv_CourseStatusValue)
    TextView tv_CourseStatusValue;
    //endregion


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        int TranscriptModelIndex = bundle.getInt("transcriptModels");

        tv_CourseCodeValue.setText(bundle.getString(Constants.COURSE_CODE));
        tv_CourseNameValue.setText(bundle.getString(Constants.COURSE_DESCRIPTION_ARABIC));
        tv_CourseCreditHourValue.setText(bundle.getString(Constants.COURSE_CREDIT_HOUR));
        tv_CourseGradeValue.setText(bundle.getString(Constants.COURSE_GRADE));
        tv_CourseStatusValue.setText(bundle.getString(Constants.COURSE_STATUS));
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_course_details;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean isEnableTitle() {
        return false;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected int getTitleResource() {
        return R.string.textView_text_courseDetails_title;
    }


    @Override
    protected boolean isEnableHome() {
        return false;
    }

    @Override
    public void onClick(View view) {
    }


}
