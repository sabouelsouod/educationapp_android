
package com.example.informatque.education.models;

import com.google.gson.annotations.SerializedName;

public class TranscriptModel {

    @SerializedName("ABS_FLG")
    private String mABSFLG;
    @SerializedName("ACAD_WARN_TYPE_DESCR_AR")
    private String mACADWARNTYPEDESCRAR;
    @SerializedName("ACAD_WARN_TYPE_DESCR_EN")
    private String mACADWARNTYPEDESCREN;
    @SerializedName("ACAD_YEAR_DESCR_AR")
    private String mACADYEARDESCRAR;
    @SerializedName("ACAD_YEAR_DESCR_EN")
    private String mACADYEARDESCREN;
    @SerializedName("ACCUM_CH")
    private String mACCUMCH;
    @SerializedName("ACCUM_CH_TOT")
    private String mACCUMCHTOT;
    @SerializedName("ACCUM_GPA")
    private String mACCUMGPA;
    @SerializedName("ACCUM_POINT")
    private String mACCUMPOINT;
    @SerializedName("ACCUM_POINT_TOT")
    private String mACCUMPOINTTOT;
    @SerializedName("ADD_GPA_FLG")
    private String mADDGPAFLG;
    @SerializedName("adm_sem_ar")
    private String mAdmSemAr;
    @SerializedName("adm_sem_en")
    private String mAdmSemEn;
    @SerializedName("adm_year_ar")
    private String mAdmYearAr;
    @SerializedName("adm_year_en")
    private String mAdmYearEn;
    @SerializedName("COURSE_CODE")
    private String mCOURSECODE;
    @SerializedName("COURSE_DEGREE")
    private String mCOURSEDEGREE;
    @SerializedName("COURSE_DESCR_AR")
    private String mCOURSEDESCRAR;
    @SerializedName("COURSE_DESCR_EN")
    private String mCOURSEDESCREN;
    @SerializedName("COURSE_POINT")
    private String mCOURSEPOINT;
    @SerializedName("CREDIT_HOURS")
    private String mCREDITHOURS;
    @SerializedName("CRS_NO")
    private String mCRSNO;
    @SerializedName("ED_ACAD_YEAR_ID")
    private String mEDACADYEARID;
    @SerializedName("ED_CODE_COURSE_STATUS_ID")
    private String mEDCODECOURSESTATUSID;
    @SerializedName("ED_CODE_ENROLL_TYPE_ID")
    private String mEDCODEENROLLTYPEID;
    @SerializedName("ED_CODE_SEMESTER_ID")
    private String mEDCODESEMESTERID;
    @SerializedName("ED_STUD_ID")
    private String mEDSTUDID;
    @SerializedName("ED_STUD_SEMESTER_ID")
    private String mEDSTUDSEMESTERID;
    @SerializedName("ENROLL_AR")
    private String mENROLLAR;
    @SerializedName("ENROLL_EN")
    private String mENROLLEN;
    @SerializedName("Enroll_Date")
    private String mEnrollDate;
    @SerializedName("FULL_NAME_AR")
    private String mFULLNAMEAR;
    @SerializedName("FULL_NAME_EN")
    private String mFULLNAMEEN;
    @SerializedName("GRADING_AR")
    private String mGRADINGAR;
    @SerializedName("GRADING_EN")
    private String mGRADINGEN;
    @SerializedName("GS_CODE_PASS_FAIL_ID")
    private String mGSCODEPASSFAILID;
    @SerializedName("HOLD_FOR_GRAD_FLG")
    private String mHOLDFORGRADFLG;
    @SerializedName("IS_RESULT_APPROVED")
    private String mISRESULTAPPROVED;
    @SerializedName("MAJOR_CGPA")
    private String mMAJORCGPA;
    @SerializedName("PF_FLG")
    private String mPFFLG;
    @SerializedName("REPEAT_FLG")
    private String mREPEATFLG;
    @SerializedName("REPEAT_NO")
    private String mREPEATNO;
    @SerializedName("RegAccumCH")
    private String mRegAccumCH;
    @SerializedName("RegSemCH")
    private String mRegSemCH;
    @SerializedName("SEM_CH")
    private String mSEMCH;
    @SerializedName("SEMESTER_DESCR_AR")
    private String mSEMESTERDESCRAR;
    @SerializedName("SEMESTER_DESCR_EN")
    private String mSEMESTERDESCREN;
    @SerializedName("SEMESTER_ORDER")
    private String mSEMESTERORDER;
    @SerializedName("SEM_GPA")
    private String mSEMGPA;
    @SerializedName("SEM_POINT")
    private String mSEMPOINT;
    @SerializedName("STATES_AR")
    private String mSTATESAR;
    @SerializedName("STATES_EN")
    private String mSTATESEN;
    @SerializedName("STATUS_DESCR_AR")
    private String mSTATUSDESCRAR;
    @SerializedName("STATUS_DESCR_EN")
    private String mSTATUSDESCREN;
    @SerializedName("STUD_FACULTY_CODE")
    private String mSTUDFACULTYCODE;
    @SerializedName("SYMBOL")
    private String mSYMBOL;
    @SerializedName("Stf_name_ar")
    private String mStfNameAr;
    @SerializedName("Stf_name_en")
    private String mStfNameEn;
    @SerializedName("Symbol_AR")
    private String mSymbolAR;
    @SerializedName("Symbol_EN")
    private String mSymbolEN;
    @SerializedName("YEAR_ORDER")
    private String mYEARORDER;

    public String getABSFLG() {
        return mABSFLG;
    }

    public void setABSFLG(String ABSFLG) {
        mABSFLG = ABSFLG;
    }

    public String getACADWARNTYPEDESCRAR() {
        return mACADWARNTYPEDESCRAR;
    }

    public void setACADWARNTYPEDESCRAR(String ACADWARNTYPEDESCRAR) {
        mACADWARNTYPEDESCRAR = ACADWARNTYPEDESCRAR;
    }

    public String getACADWARNTYPEDESCREN() {
        return mACADWARNTYPEDESCREN;
    }

    public void setACADWARNTYPEDESCREN(String ACADWARNTYPEDESCREN) {
        mACADWARNTYPEDESCREN = ACADWARNTYPEDESCREN;
    }

    public String getACADYEARDESCRAR() {
        return mACADYEARDESCRAR;
    }

    public void setACADYEARDESCRAR(String ACADYEARDESCRAR) {
        mACADYEARDESCRAR = ACADYEARDESCRAR;
    }

    public String getACADYEARDESCREN() {
        return mACADYEARDESCREN;
    }

    public void setACADYEARDESCREN(String ACADYEARDESCREN) {
        mACADYEARDESCREN = ACADYEARDESCREN;
    }

    public String getACCUMCH() {
        return mACCUMCH;
    }

    public void setACCUMCH(String ACCUMCH) {
        mACCUMCH = ACCUMCH;
    }

    public String getACCUMCHTOT() {
        return mACCUMCHTOT;
    }

    public void setACCUMCHTOT(String ACCUMCHTOT) {
        mACCUMCHTOT = ACCUMCHTOT;
    }

    public String getACCUMGPA() {
        return mACCUMGPA;
    }

    public void setACCUMGPA(String ACCUMGPA) {
        mACCUMGPA = ACCUMGPA;
    }

    public String getACCUMPOINT() {
        return mACCUMPOINT;
    }

    public void setACCUMPOINT(String ACCUMPOINT) {
        mACCUMPOINT = ACCUMPOINT;
    }

    public String getACCUMPOINTTOT() {
        return mACCUMPOINTTOT;
    }

    public void setACCUMPOINTTOT(String ACCUMPOINTTOT) {
        mACCUMPOINTTOT = ACCUMPOINTTOT;
    }

    public String getADDGPAFLG() {
        return mADDGPAFLG;
    }

    public void setADDGPAFLG(String ADDGPAFLG) {
        mADDGPAFLG = ADDGPAFLG;
    }

    public String getAdmSemAr() {
        return mAdmSemAr;
    }

    public void setAdmSemAr(String admSemAr) {
        mAdmSemAr = admSemAr;
    }

    public String getAdmSemEn() {
        return mAdmSemEn;
    }

    public void setAdmSemEn(String admSemEn) {
        mAdmSemEn = admSemEn;
    }

    public String getAdmYearAr() {
        return mAdmYearAr;
    }

    public void setAdmYearAr(String admYearAr) {
        mAdmYearAr = admYearAr;
    }

    public String getAdmYearEn() {
        return mAdmYearEn;
    }

    public void setAdmYearEn(String admYearEn) {
        mAdmYearEn = admYearEn;
    }

    public String getCOURSECODE() {
        return mCOURSECODE;
    }

    public void setCOURSECODE(String COURSECODE) {
        mCOURSECODE = COURSECODE;
    }

    public String getCOURSEDEGREE() {
        return mCOURSEDEGREE;
    }

    public void setCOURSEDEGREE(String COURSEDEGREE) {
        mCOURSEDEGREE = COURSEDEGREE;
    }

    public String getCOURSEDESCRAR() {
        return mCOURSEDESCRAR;
    }

    public void setCOURSEDESCRAR(String COURSEDESCRAR) {
        mCOURSEDESCRAR = COURSEDESCRAR;
    }

    public String getCOURSEDESCREN() {
        return mCOURSEDESCREN;
    }

    public void setCOURSEDESCREN(String COURSEDESCREN) {
        mCOURSEDESCREN = COURSEDESCREN;
    }

    public String getCOURSEPOINT() {
        return mCOURSEPOINT;
    }

    public void setCOURSEPOINT(String COURSEPOINT) {
        mCOURSEPOINT = COURSEPOINT;
    }

    public String getCREDITHOURS() {
        return mCREDITHOURS;
    }

    public void setCREDITHOURS(String CREDITHOURS) {
        mCREDITHOURS = CREDITHOURS;
    }

    public String getCRSNO() {
        return mCRSNO;
    }

    public void setCRSNO(String CRSNO) {
        mCRSNO = CRSNO;
    }

    public String getEDACADYEARID() {
        return mEDACADYEARID;
    }

    public void setEDACADYEARID(String EDACADYEARID) {
        mEDACADYEARID = EDACADYEARID;
    }

    public String getEDCODECOURSESTATUSID() {
        return mEDCODECOURSESTATUSID;
    }

    public void setEDCODECOURSESTATUSID(String EDCODECOURSESTATUSID) {
        mEDCODECOURSESTATUSID = EDCODECOURSESTATUSID;
    }

    public String getEDCODEENROLLTYPEID() {
        return mEDCODEENROLLTYPEID;
    }

    public void setEDCODEENROLLTYPEID(String EDCODEENROLLTYPEID) {
        mEDCODEENROLLTYPEID = EDCODEENROLLTYPEID;
    }

    public String getEDCODESEMESTERID() {
        return mEDCODESEMESTERID;
    }

    public void setEDCODESEMESTERID(String EDCODESEMESTERID) {
        mEDCODESEMESTERID = EDCODESEMESTERID;
    }

    public String getEDSTUDID() {
        return mEDSTUDID;
    }

    public void setEDSTUDID(String EDSTUDID) {
        mEDSTUDID = EDSTUDID;
    }

    public String getEDSTUDSEMESTERID() {
        return mEDSTUDSEMESTERID;
    }

    public void setEDSTUDSEMESTERID(String EDSTUDSEMESTERID) {
        mEDSTUDSEMESTERID = EDSTUDSEMESTERID;
    }

    public String getENROLLAR() {
        return mENROLLAR;
    }

    public void setENROLLAR(String ENROLLAR) {
        mENROLLAR = ENROLLAR;
    }

    public String getENROLLEN() {
        return mENROLLEN;
    }

    public void setENROLLEN(String ENROLLEN) {
        mENROLLEN = ENROLLEN;
    }

    public String getEnrollDate() {
        return mEnrollDate;
    }

    public void setEnrollDate(String EnrollDate) {
        mEnrollDate = EnrollDate;
    }

    public String getFULLNAMEAR() {
        return mFULLNAMEAR;
    }

    public void setFULLNAMEAR(String FULLNAMEAR) {
        mFULLNAMEAR = FULLNAMEAR;
    }

    public String getFULLNAMEEN() {
        return mFULLNAMEEN;
    }

    public void setFULLNAMEEN(String FULLNAMEEN) {
        mFULLNAMEEN = FULLNAMEEN;
    }

    public String getGRADINGAR() {
        return mGRADINGAR;
    }

    public void setGRADINGAR(String GRADINGAR) {
        mGRADINGAR = GRADINGAR;
    }

    public String getGRADINGEN() {
        return mGRADINGEN;
    }

    public void setGRADINGEN(String GRADINGEN) {
        mGRADINGEN = GRADINGEN;
    }

    public String getGSCODEPASSFAILID() {
        return mGSCODEPASSFAILID;
    }

    public void setGSCODEPASSFAILID(String GSCODEPASSFAILID) {
        mGSCODEPASSFAILID = GSCODEPASSFAILID;
    }

    public String getHOLDFORGRADFLG() {
        return mHOLDFORGRADFLG;
    }

    public void setHOLDFORGRADFLG(String HOLDFORGRADFLG) {
        mHOLDFORGRADFLG = HOLDFORGRADFLG;
    }

    public String getISRESULTAPPROVED() {
        return mISRESULTAPPROVED;
    }

    public void setISRESULTAPPROVED(String ISRESULTAPPROVED) {
        mISRESULTAPPROVED = ISRESULTAPPROVED;
    }

    public String getMAJORCGPA() {
        return mMAJORCGPA;
    }

    public void setMAJORCGPA(String MAJORCGPA) {
        mMAJORCGPA = MAJORCGPA;
    }

    public String getPFFLG() {
        return mPFFLG;
    }

    public void setPFFLG(String PFFLG) {
        mPFFLG = PFFLG;
    }

    public String getREPEATFLG() {
        return mREPEATFLG;
    }

    public void setREPEATFLG(String REPEATFLG) {
        mREPEATFLG = REPEATFLG;
    }

    public String getREPEATNO() {
        return mREPEATNO;
    }

    public void setREPEATNO(String REPEATNO) {
        mREPEATNO = REPEATNO;
    }

    public String getRegAccumCH() {
        return mRegAccumCH;
    }

    public void setRegAccumCH(String RegAccumCH) {
        mRegAccumCH = RegAccumCH;
    }

    public String getRegSemCH() {
        return mRegSemCH;
    }

    public void setRegSemCH(String RegSemCH) {
        mRegSemCH = RegSemCH;
    }

    public String getSEMCH() {
        return mSEMCH;
    }

    public void setSEMCH(String SEMCH) {
        mSEMCH = SEMCH;
    }

    public String getSEMESTERDESCRAR() {
        return mSEMESTERDESCRAR;
    }

    public void setSEMESTERDESCRAR(String SEMESTERDESCRAR) {
        mSEMESTERDESCRAR = SEMESTERDESCRAR;
    }

    public String getSEMESTERDESCREN() {
        return mSEMESTERDESCREN;
    }

    public void setSEMESTERDESCREN(String SEMESTERDESCREN) {
        mSEMESTERDESCREN = SEMESTERDESCREN;
    }

    public String getSEMESTERORDER() {
        return mSEMESTERORDER;
    }

    public void setSEMESTERORDER(String SEMESTERORDER) {
        mSEMESTERORDER = SEMESTERORDER;
    }

    public String getSEMGPA() {
        return mSEMGPA;
    }

    public void setSEMGPA(String SEMGPA) {
        mSEMGPA = SEMGPA;
    }

    public String getSEMPOINT() {
        return mSEMPOINT;
    }

    public void setSEMPOINT(String SEMPOINT) {
        mSEMPOINT = SEMPOINT;
    }

    public String getSTATESAR() {
        return mSTATESAR;
    }

    public void setSTATESAR(String STATESAR) {
        mSTATESAR = STATESAR;
    }

    public String getSTATESEN() {
        return mSTATESEN;
    }

    public void setSTATESEN(String STATESEN) {
        mSTATESEN = STATESEN;
    }

    public String getSTATUSDESCRAR() {
        return mSTATUSDESCRAR;
    }

    public void setSTATUSDESCRAR(String STATUSDESCRAR) {
        mSTATUSDESCRAR = STATUSDESCRAR;
    }

    public String getSTATUSDESCREN() {
        return mSTATUSDESCREN;
    }

    public void setSTATUSDESCREN(String STATUSDESCREN) {
        mSTATUSDESCREN = STATUSDESCREN;
    }

    public String getSTUDFACULTYCODE() {
        return mSTUDFACULTYCODE;
    }

    public void setSTUDFACULTYCODE(String STUDFACULTYCODE) {
        mSTUDFACULTYCODE = STUDFACULTYCODE;
    }

    public String getSYMBOL() {
        return mSYMBOL;
    }

    public void setSYMBOL(String SYMBOL) {
        mSYMBOL = SYMBOL;
    }

    public String getStfNameAr() {
        return mStfNameAr;
    }

    public void setStfNameAr(String StfNameAr) {
        mStfNameAr = StfNameAr;
    }

    public String getStfNameEn() {
        return mStfNameEn;
    }

    public void setStfNameEn(String StfNameEn) {
        mStfNameEn = StfNameEn;
    }

    public String getSymbolAR() {
        return mSymbolAR;
    }

    public void setSymbolAR(String SymbolAR) {
        mSymbolAR = SymbolAR;
    }

    public String getSymbolEN() {
        return mSymbolEN;
    }

    public void setSymbolEN(String SymbolEN) {
        mSymbolEN = SymbolEN;
    }

    public String getYEARORDER() {
        return mYEARORDER;
    }

    public void setYEARORDER(String YEARORDER) {
        mYEARORDER = YEARORDER;
    }

}
