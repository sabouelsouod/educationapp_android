
package com.example.informatque.education.models;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class FacultyModel {

    @SerializedName("AsFacultyInfoId")
    private int mAsFacultyInfoId;
    @SerializedName("Departments")
    private List<DepartmentModel> mDepartmentModels;
    @SerializedName("Id")
    private int mId;
    @SerializedName("Majors")
    private Object mMajors;
    @SerializedName("NameAr")
    private String mNameAr;
    @SerializedName("NameEn")
    private String mNameEn;
    @SerializedName("Type")
    private int mType;

    public int getAsFacultyInfoId() {
        return mAsFacultyInfoId;
    }

    public void setAsFacultyInfoId(int AsFacultyInfoId) {
        mAsFacultyInfoId = AsFacultyInfoId;
    }

    public List<DepartmentModel> getDepartments() {
        return mDepartmentModels;
    }

    public void setDepartments(List<DepartmentModel> departmentModels) {
        mDepartmentModels = departmentModels;
    }

    public int getId() {
        return mId;
    }

    public void setId(int Id) {
        mId = Id;
    }

    public Object getMajors() {
        return mMajors;
    }

    public void setMajors(Object Majors) {
        mMajors = Majors;
    }

    public String getNameAr() {
        return mNameAr;
    }

    public void setNameAr(String NameAr) {
        mNameAr = NameAr;
    }

    public String getNameEn() {
        return mNameEn;
    }

    public void setNameEn(String NameEn) {
        mNameEn = NameEn;
    }

    public int getType() {
        return mType;
    }

    public void setType(int Type) {
        mType = Type;
    }

}
