package com.example.informatque.education.viewControllers;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.informatque.education.R;
import com.example.informatque.education.helpers.ParentActivity;
import com.example.informatque.education.helpers.Updatable;
import com.example.informatque.education.helpers.retrofit.WebServices;
import com.example.informatque.education.models.CoursesRegestrationModel;
import com.example.informatque.education.models.SemesterModel;
import com.example.informatque.education.utils.Constants;
import com.example.informatque.education.utils.MySharedPreferences;
import com.example.informatque.education.viewModels.CourseRegestrationViewModel;
import com.example.informatque.education.viewModels.LoginViewModel;
import com.example.informatque.education.viewModels.SchoolRecordViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CoursesRegistrationViewController extends ParentActivity implements Updatable {

    @BindView(R.id.coursesRegistration)
    LinearLayout coursesRegistration;

    @BindView(R.id.coursesList)
    LinearLayout coursesList;


    String languageCode;
    MySharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);

        preferences = new MySharedPreferences(this);

        languageCode = preferences.getData(Constants.LANGUAGE_CODE);

        String StudId = LoginViewModel.getInstance().getUserData().getStudentID();
        String SemesterId = LoginViewModel.getInstance().getUserData().getSemesterCode();
        String YearId = LoginViewModel.getInstance().getUserData().getAcademicYear();

        CourseRegestrationViewModel.getInstance().getDAta(StudId, YearId, SemesterId, CoursesRegistrationViewController.this, CoursesRegistrationViewController.this);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_course_regestration;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean isEnableHome() {
        return false;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isEnableTitle() {
        return false;
    }

    @Override
    protected int getTitleResource() {
        return R.string.textView_text_coursesRegistration_title;
    }

    @Override
    public void onClick(View view) {

    }

    void drawCoursesCard() {

        final ArrayList<CoursesRegestrationModel> coursesRegestrationModels = (ArrayList) CourseRegestrationViewModel.getInstance().getCoursesRegestrationModels();

        for (int i = 0; i < coursesRegestrationModels.size(); i++) {

            View courseRow = LayoutInflater.from(getBaseContext()).inflate(R.layout.course_row, null);
            View separator = courseRow.findViewById(R.id.separator);
            final TextView tv_CourseCodeValue = (TextView) courseRow.findViewById(R.id.tv_CourseCodeValue);
            final TextView tv_CourseDescriptionValue = (TextView) courseRow.findViewById(R.id.tv_CourseDescriptionValue);

            tv_CourseCodeValue.setText(coursesRegestrationModels.get(i).getCOURSECODE());

            if (preferences.getData(Constants.LANGUAGE_CODE).equals(Constants.ENGLISH_CODE)) {

                tv_CourseDescriptionValue.setText(coursesRegestrationModels.get(i).getCOURSEDESCREN());

            } else {

                tv_CourseDescriptionValue.setText(coursesRegestrationModels.get(i).getCOURSEDESCRAR());

            }


            final int index = i;

            //Add Listener to the Single Course
            courseRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(getBaseContext(), CourseRegestrationDetailsViewController.class);
                    Bundle bundle = new Bundle();

                    bundle.putString(Constants.COURSE_CODE, coursesRegestrationModels.get(index).getCOURSECODE());
                    if (preferences.getData(Constants.LANGUAGE_CODE).equals(Constants.ENGLISH_CODE)) {

                        bundle.putString(Constants.COURSE_DESCRIPTION_ARABIC, coursesRegestrationModels.get(index).getCOURSEDESCREN());

                    } else {

                        bundle.putString(Constants.COURSE_DESCRIPTION_ARABIC, coursesRegestrationModels.get(index).getCOURSEDESCRAR());

                    }

                    bundle.putString(Constants.COURSE_CREDIT_HOUR, coursesRegestrationModels.get(index).getCREDITHOURS());
                    bundle.putString(Constants.COURSE_STATUS_COURSES, coursesRegestrationModels.get(index).getSYMBOL());

                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });

            if (i == coursesRegestrationModels.size() - 1)
                separator.setVisibility(View.GONE);

            coursesList.addView(courseRow);
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();

    }

    @Override
    public void update(WebServices apiName) {

        if (apiName.equals(WebServices.CourseRegestration)) {

            drawCoursesCard();

        } else {
        }


    }

    @Override
    public void updateRefresh(WebServices method) {

    }
}
