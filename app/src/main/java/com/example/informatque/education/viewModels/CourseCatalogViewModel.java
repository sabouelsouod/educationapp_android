package com.example.informatque.education.viewModels;

import android.content.Context;
import android.util.Log;

import com.example.informatque.education.EducationAppController;
import com.example.informatque.education.helpers.Updatable;
import com.example.informatque.education.helpers.retrofit.EducationServices;
import com.example.informatque.education.helpers.retrofit.WebServices;
import com.example.informatque.education.models.AcademicYearModel;
import com.example.informatque.education.models.CourseTypeModel;
import com.example.informatque.education.models.FacultyModel;
import com.example.informatque.education.utils.Utils;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CourseCatalogViewModel {

    private static final String TAG = CourseCatalogViewModel.class.getSimpleName();

    private Updatable updatable;
    public static CourseCatalogViewModel instance = new CourseCatalogViewModel();


    @SerializedName(value = "result")
    String result;

    @SerializedName(value = "details")
    String details;

    @SerializedName(value = "CourseTypes")
    ArrayList<CourseTypeModel> CourseType;

    @SerializedName(value = "Faculties")
    ArrayList<FacultyModel> Faculties;

    public String getResult() {
        return result;
    }

    public String getDetails() {
        return details;
    }

    public ArrayList<CourseTypeModel> getCourseType() {
        return CourseType;
    }

    public ArrayList<FacultyModel> getFaculties() {
        return Faculties;
    }

    public Context context;


    public static CourseCatalogViewModel getInstance() {
        if (instance == null)
            instance = new CourseCatalogViewModel();
        return instance;
    }

    /*---------------------------------------------------*/
    public void getData(Context context, final Updatable updatable) {
        this.updatable = updatable;
        this.context = context;


        EducationAppController application = EducationAppController.get(context);
        EducationServices educationServices = application.getJMTServices();


        Call<CourseCatalogViewModel> call = educationServices.CourseCatalogFilterData();
        call.enqueue(new Callback<CourseCatalogViewModel>() {
            @Override
            public void onResponse(Call<CourseCatalogViewModel> call, Response<CourseCatalogViewModel> response) {
                onSuccess(response);
            }

            @Override
            public void onFailure(Call<CourseCatalogViewModel> call, Throwable t) {
                onError();
                Log.e(TAG, "onFailure");
                Log.e(TAG, "Throwable" + t.getMessage());
            }

        });


    }


    /*---------------------------------------------------*/
    private void onSuccess(Response<CourseCatalogViewModel> response) {
        // TODO: 11/15/2017 Mapping Response Data To View Model Singleton object
        instance = response.body();
        if (instance != null) {
            updatable.update(WebServices.CourseCAtalog);
        }
    }

    private void onError() {

        updatable.update(WebServices.CourseCAtalog);
    }

}
