package com.example.informatque.education;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.example.informatque.education.helpers.retrofit.EducationServices;
import com.example.informatque.education.helpers.retrofit.RetrofitClient;


public class EducationAppController extends Application {

    public static final String TAG = EducationAppController.class.getSimpleName();
    private static Context context;
    private EducationServices educationServices;
    private static boolean activityVisible;


    @Override
    public void onCreate() {
        super.onCreate();

        context = this;
    }

    public static EducationAppController get(Context context) {
        return (EducationAppController) context.getApplicationContext();
    }

    public static EducationAppController create(Context context) {
        return EducationAppController.get(context);
    }

    public static Context getContext() {
        return context;
    }

    public EducationServices getJMTServices() {
        if (educationServices == null) {
            educationServices = RetrofitClient.create();
        }

        return educationServices;
    }

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}
