package com.example.informatque.education.helpers;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.informatque.education.R;
import com.example.informatque.education.utils.Utility;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public abstract class ParentActivity extends AppCompatActivity implements View.OnClickListener {

    protected AppCompatActivity activity;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        TextView tv_Title = (TextView) findViewById(R.id.toolbar_title);


//        Fabric.with(this, new Crashlytics());
        if (isEnableToolbar()) {

            if (isEnableTitle()) {

                tv_Title.setText("");
                configureToolbar();


            } else

                tv_Title.setText(getTitleResource());
            configureToolbar();

        }

        activity = this;
    }

    protected abstract int getLayoutResource();

    protected abstract boolean isEnableBack();

    protected abstract boolean isEnableHome();

    protected abstract boolean isEnableToolbar();

    protected abstract int getTitleResource();

    protected abstract boolean isEnableTitle();


    // init the view_toolbar
    private void configureToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        LinearLayout home_button = (LinearLayout) findViewById(R.id.btn_menu);
        home_button.setBackgroundResource(R.drawable.selector_home_btn);


        // check the view_toolbar
        if (toolbar != null) {

            // view_toolbar is available >> handle it
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("");
            // check if enable back
            if (isEnableBack()) {
                // set the back icon

                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
                //toolbar.setNavigationIcon(android.R.drawable.);
            } else if (isEnableHome()) {


                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                home_button.setVisibility(View.VISIBLE);




            }


        }
    }


    public void showProgressDialog() {
        progressDialog = Utility.showProgressDialog(this, R.string.progressDialog_message);
    }

    public void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


}
