
package com.example.informatque.education.models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class FacultyAcademicModel {

    @SerializedName("AsFacultyInfoId")
    private int mAsFacultyInfoId;
    @SerializedName("Departments")
    private Object mDepartments;
    @SerializedName("Id")
    private int mId;
    @SerializedName("Majors")
    private Object mMajors;
    @SerializedName("NameAr")
    private String mNameAr;
    @SerializedName("NameEn")
    private String mNameEn;
    @SerializedName("Type")
    private int mType;

    public int getAsFacultyInfoId() {
        return mAsFacultyInfoId;
    }

    public void setAsFacultyInfoId(int AsFacultyInfoId) {
        mAsFacultyInfoId = AsFacultyInfoId;
    }

    public Object getDepartments() {
        return mDepartments;
    }

    public void setDepartments(Object Departments) {
        mDepartments = Departments;
    }

    public int getId() {
        return mId;
    }

    public void setId(int Id) {
        mId = Id;
    }

    public Object getMajors() {
        return mMajors;
    }

    public void setMajors(Object Majors) {
        mMajors = Majors;
    }

    public String getNameAr() {
        return mNameAr;
    }

    public void setNameAr(String NameAr) {
        mNameAr = NameAr;
    }

    public String getNameEn() {
        return mNameEn;
    }

    public void setNameEn(String NameEn) {
        mNameEn = NameEn;
    }

    public int getType() {
        return mType;
    }

    public void setType(int Type) {
        mType = Type;
    }

}
