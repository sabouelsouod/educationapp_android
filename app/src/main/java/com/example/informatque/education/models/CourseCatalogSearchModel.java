
package com.example.informatque.education.models;

import javax.annotation.Generated;

import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CourseCatalogSearchModel {

    @SerializedName("AS_FACULTY_INFO_ID")
    private String mASFACULTYINFOID;
    @SerializedName("COURSE_CODE")
    private String mCOURSECODE;
    @SerializedName("COURSE_CONTENTS_AR")
    private String mCOURSECONTENTSAR;
    @SerializedName("COURSE_DESCR_AR")
    private String mCOURSEDESCRAR;
    @SerializedName("COURSE_DESCR_EN")
    private String mCOURSEDESCREN;
    @SerializedName("CREDIT_HOURS")
    private String mCREDITHOURS;
    @SerializedName("FACULTY_DESCR_AR")
    private String mFACULTYDESCRAR;
    @SerializedName("FACULTY_DESCR_EN")
    private String mFACULTYDESCREN;

    public String getASFACULTYINFOID() {
        return mASFACULTYINFOID;
    }

    public void setASFACULTYINFOID(String ASFACULTYINFOID) {
        mASFACULTYINFOID = ASFACULTYINFOID;
    }

    public String getCOURSECODE() {
        return mCOURSECODE;
    }

    public void setCOURSECODE(String COURSECODE) {
        mCOURSECODE = COURSECODE;
    }

    public String getCOURSECONTENTSAR() {
        return mCOURSECONTENTSAR;
    }

    public void setCOURSECONTENTSAR(String COURSECONTENTSAR) {
        mCOURSECONTENTSAR = COURSECONTENTSAR;
    }

    public String getCOURSEDESCRAR() {
        return mCOURSEDESCRAR;
    }

    public void setCOURSEDESCRAR(String COURSEDESCRAR) {
        mCOURSEDESCRAR = COURSEDESCRAR;
    }

    public String getCOURSEDESCREN() {
        return mCOURSEDESCREN;
    }

    public void setCOURSEDESCREN(String COURSEDESCREN) {
        mCOURSEDESCREN = COURSEDESCREN;
    }

    public String getCREDITHOURS() {
        return mCREDITHOURS;
    }

    public void setCREDITHOURS(String CREDITHOURS) {
        mCREDITHOURS = CREDITHOURS;
    }

    public String getFACULTYDESCRAR() {
        return mFACULTYDESCRAR;
    }

    public void setFACULTYDESCRAR(String FACULTYDESCRAR) {
        mFACULTYDESCRAR = FACULTYDESCRAR;
    }

    public String getFACULTYDESCREN() {
        return mFACULTYDESCREN;
    }

    public void setFACULTYDESCREN(String FACULTYDESCREN) {
        mFACULTYDESCREN = FACULTYDESCREN;
    }

}
