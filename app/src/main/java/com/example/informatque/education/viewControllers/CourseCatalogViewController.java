package com.example.informatque.education.viewControllers;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.informatque.education.Adapters.CourseCatalogSearchAdapter;
import com.example.informatque.education.R;
import com.example.informatque.education.helpers.ParentActivity;
import com.example.informatque.education.helpers.Updatable;
import com.example.informatque.education.helpers.retrofit.WebServices;
import com.example.informatque.education.models.CourseCatalogSearchModel;
import com.example.informatque.education.models.DepartmentModel;
import com.example.informatque.education.models.FacultyModel;
import com.example.informatque.education.utils.Constants;
import com.example.informatque.education.utils.MySharedPreferences;
import com.example.informatque.education.utils.Utility;
import com.example.informatque.education.viewModels.CourseCatalogViewModel;
import com.example.informatque.education.viewModels.SearchCourseCatalogViewModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CourseCatalogViewController extends ParentActivity implements Updatable {

    //regionBindViews

    //regionRecycleView
    @BindView(R.id.recyclerViewCourseCatalog)
    RecyclerView recyclerView;
    @BindView(R.id.transitions_container)
    LinearLayout mainCourseCatalogContainer;
    //endregion

    //regionSpinners
    @BindView(R.id.spinner_course_style_english)
    Spinner spinnerCourse;

    @BindView(R.id.sp_college)
    Spinner spinnerFaculties;


    @BindView(R.id.sp_Department)
    Spinner spinnerDepartments;
    //endregion

    //regionRadioButton
    @BindView(R.id.activeRadioGroup)
    RadioGroup activeRadioGroup;

    @BindView(R.id.degreeRadioGroup)
    RadioGroup degreeRadioGroup;

    @BindView(R.id.onlineRadioGroup)
    RadioGroup onlineRadioGroup;
    //endregion

    @BindView(R.id.courseCodeEt)
    EditText courseCodeEt;

    @BindView(R.id.courseTitleEt)
    EditText courseTitleEt;


    @BindView(R.id.btnSearch)
    Button searchBtn;

    //endregion

    //An ArrayList for Spinner Items
    ArrayList<String> courseTypesArrayList = new ArrayList<>();
    ArrayList<String> facultiesArrayList = new ArrayList<>();
    ArrayList<String> departmentsArrayList = new ArrayList<>();
    ArrayList<CourseCatalogSearchModel> courseCatalogSearchModels = new ArrayList<>();
    ArrayAdapter<String> spinnerFacultiesAdapter, spinnerCourseAdapter, spinnerDepartmentsAdapter;

    CourseCatalogSearchAdapter adapter;

    int EdCodeCourseId = 0;
    int AsFacultyInfoId = 0;
    int DeptId = 0;
    int selectedActiveRG;
    int selectedOnlineRG;
    int selectedDegreeRG;

    String CrsName;

    public String getCrsCode() {
        return CrsCode;
    }

    String CrsCode;

    MySharedPreferences preferences;


    @Override
    protected boolean isEnableTitle() {
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = new MySharedPreferences(this);

        ButterKnife.bind(this);

        initUi();

        showProgressDialog();

        if (CourseCatalogViewModel.getInstance().getCourseType() == null) {

            CourseCatalogViewModel.getInstance().getData(CourseCatalogViewController.this, CourseCatalogViewController.this);

        } else {

            getSpinnersData();
            hideProgressDialog();
        }


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_course_catalog;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean isEnableHome() {
        return false;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected int getTitleResource() {
        return R.string.textView_text_course_catalog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            // TODO: 11/21/2017 SearchButton
            case R.id.btnSearch:
                if (Utility.isNetworkAvailable(this)) {

                    showProgressDialog();

                    CrsCode = courseCodeEt.getText().toString();
                    CrsName = courseTitleEt.getText().toString();

                    SearchCourseCatalogViewModel.getInstance().getSearchDAta(EdCodeCourseId, AsFacultyInfoId, DeptId
                            , CrsName, CrsCode, selectedDegreeRG, selectedActiveRG, selectedOnlineRG, CourseCatalogViewController.this, CourseCatalogViewController.this);

                } else {
                    Snackbar.make(findViewById(R.id.mainCourseCatalogContainer), R.string.network_error, Snackbar.LENGTH_SHORT).show();

                }

                break;

        }
    }

    private void fetchFilterData() {
        courseCatalogSearchModels = SearchCourseCatalogViewModel.getInstance().getCourseCatalogSearchModels();
        adapter = new CourseCatalogSearchAdapter(this, courseCatalogSearchModels);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        //todo divider color
//        recyclerView.addItemDecoration(new Utility.SimpleDividerItemDecoration(this));
    }

    private int getCheckedRadioBtn(RadioGroup radioGroup) {
        int checkedBtnId = radioGroup.getCheckedRadioButtonId();
        View radioBtn = radioGroup.findViewById(checkedBtnId);
        return radioGroup.indexOfChild(radioBtn);

    }

    private void getSpinnersData() {


        //regionCourseTypeSpinner
        for (int i = 0; i < CourseCatalogViewModel.getInstance().getCourseType().size(); i++) {

            if (preferences.getData(Constants.LANGUAGE_CODE).equals(Constants.ENGLISH_CODE)) {

                courseTypesArrayList.add(CourseCatalogViewModel.getInstance().getCourseType().get(i).getDESCREN());


            } else {

                courseTypesArrayList.add(CourseCatalogViewModel.getInstance().getCourseType().get(i).getDESCRAR());


            }

        }


        spinnerCourseAdapter = new ArrayAdapter<>(this, R.layout.spinner_background, courseTypesArrayList);
        spinnerCourseAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerCourse.setAdapter(spinnerCourseAdapter);
        spinnerCourseAdapter.notifyDataSetChanged();
        spinnerCourse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                EdCodeCourseId = CourseCatalogViewModel.getInstance().getCourseType().get(position).getEDCODECOURSEID();
                Log.i("Courseid", String.valueOf(EdCodeCourseId));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //endregion

        //regionCollageSpinner

        final ArrayList<FacultyModel> facultyModels = CourseCatalogViewModel.getInstance().getFaculties();


        for (int i = 0; i < facultyModels.size(); i++) {

            if (preferences.getData(Constants.LANGUAGE_CODE).equals(Constants.ENGLISH_CODE)) {

                facultiesArrayList.add(facultyModels.get(i).getNameEn());

            } else {

                facultiesArrayList.add(facultyModels.get(i).getNameAr());

            }


            spinnerFacultiesAdapter = new ArrayAdapter<>(this, R.layout.spinner_background, facultiesArrayList);
            spinnerFacultiesAdapter.setDropDownViewResource(R.layout.spinner_item);
            spinnerFaculties.setAdapter(spinnerFacultiesAdapter);
            spinnerFacultiesAdapter.notifyDataSetChanged();


            spinnerDepartmentsAdapter = new ArrayAdapter<>(this, R.layout.spinner_background, departmentsArrayList);
            spinnerDepartmentsAdapter.setDropDownViewResource(R.layout.spinner_item);

            spinnerFaculties.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    AsFacultyInfoId = facultyModels.get(position).getAsFacultyInfoId();

                    Log.i("facultyid", String.valueOf(AsFacultyInfoId));

                    departmentsArrayList.clear();

                    //regionDepartmentSpinner

                    final ArrayList<DepartmentModel> departmentModels = (ArrayList) CourseCatalogViewModel.getInstance().getFaculties().get(position).getDepartments();

                    for (int j = 0; j < departmentModels.size(); j++) {

                        if (preferences.getData(Constants.LANGUAGE_CODE).equals(Constants.ENGLISH_CODE)) {

                            departmentsArrayList.add(departmentModels.get(j).getNameEn());
                            spinnerDepartments.setAdapter(spinnerDepartmentsAdapter);

                        } else {

                            departmentsArrayList.add(departmentModels.get(j).getNameAr());
                            spinnerDepartments.setAdapter(spinnerDepartmentsAdapter);

                        }

                    }

                    spinnerDepartments.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            DeptId = departmentModels.get(position).getId();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    spinnerDepartmentsAdapter.notifyDataSetChanged();

                    //endregion

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }


            });


        }
        //endregion


    }

    @Override
    public void onBackPressed() {
//
        if (recyclerView.getVisibility() == View.VISIBLE) {
            recyclerView.setVisibility(View.GONE);
            mainCourseCatalogContainer.setVisibility(View.VISIBLE);
        } else
            super.onBackPressed();

    }

    @Override
    public void update(WebServices apiName) {


        if (apiName.equals(WebServices.CourseCAtalog)) {


            getSpinnersData();
            hideProgressDialog();


        } else if (apiName.equals(WebServices.CourseCatalogSearch)) {

            if (SearchCourseCatalogViewModel.getInstance().getResult().equals("Ok")) {

                mainCourseCatalogContainer.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                courseCatalogSearchModels.clear();
                fetchFilterData();
                hideProgressDialog();

            } else {

                recyclerView.setVisibility(View.GONE);
                mainCourseCatalogContainer.setVisibility(View.VISIBLE);
                hideProgressDialog();
                Toast.makeText(this, R.string.error_data, Toast.LENGTH_SHORT).show();
            }


        }

    }

    @Override
    public void updateRefresh(WebServices method) {

    }

    private void initUi() {

        //regionSpinners
        spinnerFacultiesAdapter = new ArrayAdapter<>(this, R.layout.spinner_background, facultiesArrayList);
        spinnerFacultiesAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerFaculties.setAdapter(spinnerFacultiesAdapter);
        spinnerDepartmentsAdapter = new ArrayAdapter<>(CourseCatalogViewController.this, R.layout.spinner_background, departmentsArrayList);
        spinnerDepartmentsAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerDepartments.setAdapter(spinnerDepartmentsAdapter);

        //endregion

        //regionRadioButton

        degreeRadioGroup.check(R.id.allDegreeBtn);
        activeRadioGroup.check(R.id.allActiveBtn);
        onlineRadioGroup.check(R.id.allOnlineBtn);
        selectedActiveRG = getCheckedRadioBtn(activeRadioGroup);
        selectedOnlineRG = getCheckedRadioBtn(onlineRadioGroup);
        selectedDegreeRG = getCheckedRadioBtn(degreeRadioGroup);


        if (selectedActiveRG == 0) {

            selectedActiveRG = -1;

        } else if (selectedActiveRG == 2) {

            selectedActiveRG = 0;

        } else {

            selectedActiveRG = 1;

        }

        if (selectedOnlineRG == 0) {

            selectedOnlineRG = -1;

        } else if (selectedOnlineRG == 2) {

            selectedOnlineRG = 0;

        } else {

            selectedOnlineRG = 1;

        }
        //endregion


        searchBtn.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));

    }

}