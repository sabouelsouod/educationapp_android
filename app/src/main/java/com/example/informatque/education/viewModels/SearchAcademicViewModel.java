package com.example.informatque.education.viewModels;

import android.content.Context;
import android.util.Log;

import com.example.informatque.education.EducationAppController;
import com.example.informatque.education.helpers.Updatable;
import com.example.informatque.education.helpers.retrofit.EducationServices;
import com.example.informatque.education.helpers.retrofit.WebServices;
import com.example.informatque.education.models.CourseCatalogSearchModel;
import com.example.informatque.education.models.SearchAcademicModel;
import com.example.informatque.education.utils.Utils;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SearchAcademicViewModel {

    private static final String TAG = SearchAcademicViewModel.class.getSimpleName();

    private Updatable updatable;
    public static SearchAcademicViewModel instance = new SearchAcademicViewModel();


    @SerializedName(value = "result")
    String result;

    @SerializedName(value = "details")
    String details;

    @SerializedName(value = "items")
    ArrayList<SearchAcademicModel> CourseCatalogSearchModels;

    public Context context;


    public static SearchAcademicViewModel getInstance() {
        if (instance == null)
            instance = new SearchAcademicViewModel();
        return instance;
    }

    public String getResult() {
        return result;
    }

    public String getDetails() {
        return details;
    }

    public ArrayList<SearchAcademicModel> getCourseCatalogSearchModels() {
        return CourseCatalogSearchModels;
    }

    /*---------------------------------------------------*/
    public void getSearchDAta(int EdAcadYear, int AsFacultyInfoId, int Semester,int AsCodeDegreeClassId,
                              Context context, final Updatable updatable) {
        this.updatable = updatable;
        this.context = context;


        EducationAppController application = EducationAppController.get(context);
        EducationServices educationServices = application.getJMTServices();


        JsonObject obj = new JsonObject();
        obj.addProperty("AsFacultyInfoId", AsFacultyInfoId);
        obj.addProperty("EdAcadYearId", EdAcadYear);
        obj.addProperty("EdCodeSemesterId", Semester);
        obj.addProperty("AsCodeDegreeClassId", AsCodeDegreeClassId);


        Log.i("Boady", obj.toString());


        Call<SearchAcademicViewModel> call = educationServices.SEARCH_ACADEMIC_VIEW_MODEL_CALL(obj);
        call.enqueue(new Callback<SearchAcademicViewModel>() {
            @Override
            public void onResponse(Call<SearchAcademicViewModel> call, Response<SearchAcademicViewModel> response) {
                onSuccess(response);
            }

            @Override
            public void onFailure(Call<SearchAcademicViewModel> call, Throwable t) {
                onError();
                Log.e(TAG, "onFailure");
                Log.e(TAG, "Throwable" + t.getMessage());
            }

        });


    }


    /*---------------------------------------------------*/
    private void onSuccess(Response<SearchAcademicViewModel> response) {
        // TODO: 11/15/2017 Mapping Response Data To View Model Singleton object
        instance = response.body();
        if (instance != null) {
            updatable.update(WebServices.AcademicSearch);
        }
    }

    private void onError() {
        Utils.getInstance(context).dismissProgress();
    }

}
