
package com.example.informatque.education.viewControllers;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.informatque.education.R;
import com.example.informatque.education.helpers.ParentActivity;
import com.example.informatque.education.utils.Constants;
import com.example.informatque.education.utils.MySharedPreferences;
import com.example.informatque.education.utils.Utility;


public class LanguageViewController extends ParentActivity {

    private MySharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferences = new MySharedPreferences(this);

        Button btn_Arabic = findViewById(R.id.btn_Arabic);
        Button btn_English = findViewById(R.id.btn_English);

        btn_Arabic.setOnClickListener(this);
        btn_English.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_language_view_controller;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isEnableHome() {
        return false;
    }

    @Override
    protected int getTitleResource() {
        return 0;
    }

    @Override
    protected boolean isEnableTitle() {
        return false;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_Arabic:

                Utility.setLocale(getBaseContext(), Constants.ARABIC_CODE);
                preferences.addData(Constants.LANGUAGE_CODE, Constants.ARABIC_CODE);
                startActivity(new Intent(getBaseContext(), HomeViewComtroller.class));

                break;

            case R.id.btn_English:

                Utility.setLocale(getBaseContext(), Constants.ENGLISH_CODE);
                preferences.addData(Constants.LANGUAGE_CODE, Constants.ENGLISH_CODE);
                startActivity(new Intent(getBaseContext(), HomeViewComtroller.class));

                break;
        }

//        startActivity(new Intent(getBaseContext(), HomeActivity.class));
        finish();
    }

}
