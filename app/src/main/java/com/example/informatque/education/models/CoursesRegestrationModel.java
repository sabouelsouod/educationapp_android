
package com.example.informatque.education.models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CoursesRegestrationModel {

    @SerializedName("COURSE_CODE")
    private String mCOURSECODE;
    @SerializedName("COURSE_DESCR_AR")
    private String mCOURSEDESCRAR;
    @SerializedName("COURSE_DESCR_EN")
    private String mCOURSEDESCREN;
    @SerializedName("CREDIT_HOURS")
    private String mCREDITHOURS;
    @SerializedName("CRS_SEC_ORDR")
    private String mCRSSECORDR;
    @SerializedName("DESCR_AR")
    private String mDESCRAR;
    @SerializedName("DESCR_EN")
    private String mDESCREN;
    @SerializedName("ED_CDE_TCHNG_MTHD_ID")
    private String mEDCDETCHNGMTHDID;
    @SerializedName("ED_CODE_COURSE_ID")
    private String mEDCODECOURSEID;
    @SerializedName("ED_CODE_COURSE_STATUS_ID")
    private String mEDCODECOURSESTATUSID;
    @SerializedName("ED_COURSE_ID")
    private String mEDCOURSEID;
    @SerializedName("ED_STUD_COURSE_REG_ID")
    private String mEDSTUDCOURSEREGID;
    @SerializedName("ED_STUD_GROUP_ID")
    private String mEDSTUDGROUPID;
    @SerializedName("ED_STUD_SEMESTER_ID")
    private String mEDSTUDSEMESTERID;
    @SerializedName("GROUP_DESCR_AR")
    private String mGROUPDESCRAR;
    @SerializedName("GROUP_DESCR_EN")
    private String mGROUPDESCREN;
    @SerializedName("GROUP_ORDER")
    private String mGROUPORDER;
    @SerializedName("GROUP_ORDER_DESCR")
    private String mGROUPORDERDESCR;
    @SerializedName("NOTES")
    private String mNOTES;
    @SerializedName("PAY_AMOUNT")
    private String mPAYAMOUNT;
    @SerializedName("PAY_FLG")
    private String mPAYFLG;
    @SerializedName("REPEAT_NO")
    private String mREPEATNO;
    @SerializedName("SC_SCHEDULE_DTL_ID")
    private String mSCSCHEDULEDTLID;
    @SerializedName("SE_USER_ID")
    private String mSEUSERID;
    @SerializedName("STD_CRS_CH")
    private String mSTDCRSCH;
    @SerializedName("SYMBOL")
    private String mSYMBOL;
    @SerializedName("WAIT_NO")
    private String mWAITNO;

    public String getCOURSECODE() {
        return mCOURSECODE;
    }

    public void setCOURSECODE(String COURSECODE) {
        mCOURSECODE = COURSECODE;
    }

    public String getCOURSEDESCRAR() {
        return mCOURSEDESCRAR;
    }

    public void setCOURSEDESCRAR(String COURSEDESCRAR) {
        mCOURSEDESCRAR = COURSEDESCRAR;
    }

    public String getCOURSEDESCREN() {
        return mCOURSEDESCREN;
    }

    public void setCOURSEDESCREN(String COURSEDESCREN) {
        mCOURSEDESCREN = COURSEDESCREN;
    }

    public String getCREDITHOURS() {
        return mCREDITHOURS;
    }

    public void setCREDITHOURS(String CREDITHOURS) {
        mCREDITHOURS = CREDITHOURS;
    }

    public String getCRSSECORDR() {
        return mCRSSECORDR;
    }

    public void setCRSSECORDR(String CRSSECORDR) {
        mCRSSECORDR = CRSSECORDR;
    }

    public String getDESCRAR() {
        return mDESCRAR;
    }

    public void setDESCRAR(String DESCRAR) {
        mDESCRAR = DESCRAR;
    }

    public String getDESCREN() {
        return mDESCREN;
    }

    public void setDESCREN(String DESCREN) {
        mDESCREN = DESCREN;
    }

    public String getEDCDETCHNGMTHDID() {
        return mEDCDETCHNGMTHDID;
    }

    public void setEDCDETCHNGMTHDID(String EDCDETCHNGMTHDID) {
        mEDCDETCHNGMTHDID = EDCDETCHNGMTHDID;
    }

    public String getEDCODECOURSEID() {
        return mEDCODECOURSEID;
    }

    public void setEDCODECOURSEID(String EDCODECOURSEID) {
        mEDCODECOURSEID = EDCODECOURSEID;
    }

    public String getEDCODECOURSESTATUSID() {
        return mEDCODECOURSESTATUSID;
    }

    public void setEDCODECOURSESTATUSID(String EDCODECOURSESTATUSID) {
        mEDCODECOURSESTATUSID = EDCODECOURSESTATUSID;
    }

    public String getEDCOURSEID() {
        return mEDCOURSEID;
    }

    public void setEDCOURSEID(String EDCOURSEID) {
        mEDCOURSEID = EDCOURSEID;
    }

    public String getEDSTUDCOURSEREGID() {
        return mEDSTUDCOURSEREGID;
    }

    public void setEDSTUDCOURSEREGID(String EDSTUDCOURSEREGID) {
        mEDSTUDCOURSEREGID = EDSTUDCOURSEREGID;
    }

    public String getEDSTUDGROUPID() {
        return mEDSTUDGROUPID;
    }

    public void setEDSTUDGROUPID(String EDSTUDGROUPID) {
        mEDSTUDGROUPID = EDSTUDGROUPID;
    }

    public String getEDSTUDSEMESTERID() {
        return mEDSTUDSEMESTERID;
    }

    public void setEDSTUDSEMESTERID(String EDSTUDSEMESTERID) {
        mEDSTUDSEMESTERID = EDSTUDSEMESTERID;
    }

    public String getGROUPDESCRAR() {
        return mGROUPDESCRAR;
    }

    public void setGROUPDESCRAR(String GROUPDESCRAR) {
        mGROUPDESCRAR = GROUPDESCRAR;
    }

    public String getGROUPDESCREN() {
        return mGROUPDESCREN;
    }

    public void setGROUPDESCREN(String GROUPDESCREN) {
        mGROUPDESCREN = GROUPDESCREN;
    }

    public String getGROUPORDER() {
        return mGROUPORDER;
    }

    public void setGROUPORDER(String GROUPORDER) {
        mGROUPORDER = GROUPORDER;
    }

    public String getGROUPORDERDESCR() {
        return mGROUPORDERDESCR;
    }

    public void setGROUPORDERDESCR(String GROUPORDERDESCR) {
        mGROUPORDERDESCR = GROUPORDERDESCR;
    }

    public String getNOTES() {
        return mNOTES;
    }

    public void setNOTES(String NOTES) {
        mNOTES = NOTES;
    }

    public String getPAYAMOUNT() {
        return mPAYAMOUNT;
    }

    public void setPAYAMOUNT(String PAYAMOUNT) {
        mPAYAMOUNT = PAYAMOUNT;
    }

    public String getPAYFLG() {
        return mPAYFLG;
    }

    public void setPAYFLG(String PAYFLG) {
        mPAYFLG = PAYFLG;
    }

    public String getREPEATNO() {
        return mREPEATNO;
    }

    public void setREPEATNO(String REPEATNO) {
        mREPEATNO = REPEATNO;
    }

    public String getSCSCHEDULEDTLID() {
        return mSCSCHEDULEDTLID;
    }

    public void setSCSCHEDULEDTLID(String SCSCHEDULEDTLID) {
        mSCSCHEDULEDTLID = SCSCHEDULEDTLID;
    }

    public String getSEUSERID() {
        return mSEUSERID;
    }

    public void setSEUSERID(String SEUSERID) {
        mSEUSERID = SEUSERID;
    }

    public String getSTDCRSCH() {
        return mSTDCRSCH;
    }

    public void setSTDCRSCH(String STDCRSCH) {
        mSTDCRSCH = STDCRSCH;
    }

    public String getSYMBOL() {
        return mSYMBOL;
    }

    public void setSYMBOL(String SYMBOL) {
        mSYMBOL = SYMBOL;
    }

    public String getWAITNO() {
        return mWAITNO;
    }

    public void setWAITNO(String WAITNO) {
        mWAITNO = WAITNO;
    }

}
