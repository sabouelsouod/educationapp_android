package com.example.informatque.education.Adapters;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.example.informatque.education.R;
import com.example.informatque.education.models.CourseCatalogSearchModel;
import com.example.informatque.education.models.SearchAcademicModel;
import com.example.informatque.education.utils.Constants;
import com.example.informatque.education.utils.MySharedPreferences;
import com.example.informatque.education.utils.Utility;

import java.util.ArrayList;

import butterknife.BindView;


public class AcademicCalenderSearchAdapter extends RecyclerView.Adapter<AcademicCalenderSearchAdapter.VHolder> {


    Context context;
    ArrayList<SearchAcademicModel> searchAcademicModelsList;
    private int lastPosition = -1;
    MySharedPreferences preferences;
    String languageCode;

    public AcademicCalenderSearchAdapter(Context context, ArrayList<SearchAcademicModel> searchAcademicModels) {

        this.context = context;
        this.searchAcademicModelsList = searchAcademicModels;


    }


    @Override
    public VHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.single_academic_calendar_view, parent, false);
        return new VHolder(view);
    }

    @Override
    public void onBindViewHolder(VHolder holder, int position) {

        preferences = new MySharedPreferences(context);
        languageCode = preferences.getData(Constants.LANGUAGE_CODE);


        SearchAcademicModel SingleCourse = searchAcademicModelsList.get(position);

        if (preferences.getData(Constants.LANGUAGE_CODE).equals(Constants.ENGLISH_CODE)) {

            holder.semester.setText(SingleCourse.getSEMEN());
            holder.collage.setText(SingleCourse.getENTDESCREN());
            holder.btnAcademicCalenderData.setText(SingleCourse.getACTVEN());
            holder.Actv.setText(SingleCourse.getACTVEN());


        } else {

            holder.semester.setText(SingleCourse.getSEMAR());
            holder.collage.setText(SingleCourse.getENTDESCRAR());
            holder.btnAcademicCalenderData.setText(SingleCourse.getACTVAR());
            holder.Actv.setText(SingleCourse.getACTVAR());


        }

        holder.dateFrom.setText(SingleCourse.getFROMDATE());
        holder.dateTo.setText(SingleCourse.getTODATE());

        setAnimation(holder.itemView, position);

    }


    @Override
    public int getItemCount() {

        if (searchAcademicModelsList != null && searchAcademicModelsList.size() > 0)
            return searchAcademicModelsList.size();
        else return 0;
    }

    class VHolder extends RecyclerView.ViewHolder {

        TextView semester;
        TextView collage;
        TextView dateFrom;
        TextView dateTo;
        TextView Actv;

        Button btnAcademicCalenderData;
        CardView academicCalenderDataCard;


        public VHolder(View itemView) {
            super(itemView);

            semester = itemView.findViewById(R.id.tv_Semester);
            collage = itemView.findViewById(R.id.tv_Collage);
            Actv = itemView.findViewById(R.id.ACTV);
            dateFrom = itemView.findViewById(R.id.tv_From_Date);
            dateTo = itemView.findViewById(R.id.tv_To_Date);
            btnAcademicCalenderData = itemView.findViewById(R.id.btn_academicCalenderData);
            academicCalenderDataCard = itemView.findViewById(R.id.academicCalenderDataCard);


            btnAcademicCalenderData.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (academicCalenderDataCard.getVisibility() == View.VISIBLE) {
                        academicCalenderDataCard.setVisibility(View.GONE);
                        Utility.settingArrowDrawable(btnAcademicCalenderData, languageCode, context, R.drawable.down);
                    } else {
                        academicCalenderDataCard.setVisibility(View.VISIBLE);
                        //YoYo.with(Techniques.FadeInUp).playOn(studentAcademicDataCard);
                        Utility.settingArrowDrawable(btnAcademicCalenderData, languageCode, context, R.drawable.up);
                    }
                }
            });


        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}
