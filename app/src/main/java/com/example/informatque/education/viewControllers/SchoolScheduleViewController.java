package com.example.informatque.education.viewControllers;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.alamkanak.weekview.WeekViewEvent;
import com.example.informatque.education.R;
import com.example.informatque.education.utils.MySharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class SchoolScheduleViewController extends BaseSchedualViewController {

    private MySharedPreferences preferences;

    private JSONObject scheduleJsonObject;


    Calendar startTime;

    Calendar endTime;

    WeekViewEvent event;

    List<WeekViewEvent> events = new ArrayList<WeekViewEvent>();

    @Override
    protected void onStart() {


        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferences = new MySharedPreferences(this);
//        showProgressDialog();


    }



    public List<WeekViewEvent> getEvents(int newYear, int newMonth, int Day) {

        List<WeekViewEvent> eventsMonth = new ArrayList<WeekViewEvent>();


        // TODO: 11/8/2017

        if (Day == (Calendar.SATURDAY)) {


            startTime = Calendar.getInstance();

            startTime.set(Calendar.DAY_OF_WEEK, 1);
            startTime.set(Calendar.HOUR_OF_DAY, 02);
            startTime.set(Calendar.MINUTE, 0);
            startTime.set(Calendar.MONTH, newMonth - 1);
            startTime.set(Calendar.YEAR, newYear);
            endTime = (Calendar) startTime.clone();
            endTime.set(Calendar.HOUR_OF_DAY, 5);
            endTime.set(Calendar.MINUTE, 0);
            event = new WeekViewEvent(0, getEventTitle(startTime) + getEventTitle2(endTime), startTime, endTime);
            event.setColor(getResources().getColor(R.color.colorAccent));
            eventsMonth.add(event);

        } else if (Day == (Calendar.MONDAY)) {

            startTime = Calendar.getInstance();

            startTime.set(Calendar.DAY_OF_WEEK, 1);
            startTime.set(Calendar.HOUR_OF_DAY, 05);
            startTime.set(Calendar.MINUTE, 0);
            startTime.set(Calendar.MONTH, newMonth - 1);
            startTime.set(Calendar.YEAR, newYear);
            endTime = (Calendar) startTime.clone();
            endTime.set(Calendar.HOUR_OF_DAY, 8);
            endTime.set(Calendar.MINUTE, 0);
            event = new WeekViewEvent(0, getEventTitle(startTime) + getEventTitle2(endTime), startTime, endTime);
            event.setColor(getResources().getColor(R.color.colorAccent));
            eventsMonth.add(event);
        }


        return eventsMonth;


    }


    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        int i = 1;

        if (events != null) {
            if (events.size() > 0) {
                events.clear();
            }

        }


        if (i == 1) {


            events = getEvents(newYear, newMonth - 1, Calendar.SATURDAY);
            return events;
        }

        return null;


    }

    @Override
    public void onClick(View v) {

    }





};
