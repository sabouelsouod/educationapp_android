
package com.example.informatque.education.models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CourseTypeModel {

    @SerializedName("DESCR_AR")
    private String mDESCRAR;
    @SerializedName("DESCR_EN")
    private String mDESCREN;
    @SerializedName("ED_CODE_COURSE_ID")
    private int mEDCODECOURSEID;

    public String getDESCRAR() {
        return mDESCRAR;
    }

    public void setDESCRAR(String DESCRAR) {
        mDESCRAR = DESCRAR;
    }

    public String getDESCREN() {
        return mDESCREN;
    }

    public void setDESCREN(String DESCREN) {
        mDESCREN = DESCREN;
    }

    public int getEDCODECOURSEID() {
        return mEDCODECOURSEID;
    }

    public void setEDCODECOURSEID(int EDCODECOURSEID) {
        mEDCODECOURSEID = EDCODECOURSEID;
    }

}
