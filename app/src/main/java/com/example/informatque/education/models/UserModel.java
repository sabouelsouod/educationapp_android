package com.example.informatque.education.models;//


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserModel implements Serializable  {

    @SerializedName(value = "FULL_NAME_EN")
    String studentName_EN;

    @SerializedName(value = "STUD_FACULTY_CODE")
    String studentCode;

    @SerializedName(value = "FULL_NAME_AR")
    String studentName_AR;

    @SerializedName(value = "NATION_DESCR_EN")
    String nationality_EN;

    @SerializedName(value = "NATION_DESCR_AR")
    String nationality_AR;

    @SerializedName(value = "NATIONAL_NUMBER")
    String nationalID_EN;

    @SerializedName(value = "STUD_EMAIL")
    String email;

    @SerializedName(value = "MAJOR_EN")
    String major_EN;

    @SerializedName(value = "MAJOR_AR")
    String major_AR;

    @SerializedName(value = "STUD_MOBNO")
    String phoneNo;

    @SerializedName(value = "FACULTY_DESCR_EN")
    String faculty_EN;

    @SerializedName(value = "FACULTY_DESCR_AR")
    String faculty_AR;

    @SerializedName(value = "DEGREE_EN")
    String degree_EN;
    @SerializedName(value = "DEGREE_AR")
    String degree_AR;

    @SerializedName(value = "LEVEL_AR")
    String level_AR;

    @SerializedName(value = "LEVEL_EN")
    String level_EN;

    @SerializedName(value = "SEM_CH")
    String semesterCH;

    @SerializedName(value = "SEM_GPA")
    String semesterGpa;

    @SerializedName(value = "ACCUM_GPA")
    String acumlateGpa;

    @SerializedName(value = "ACCUM_POINT")
    String acumlatePoint;

    @SerializedName(value = "ACCUM_CH")
    String acumlateCredit;

    @SerializedName(value = "GRADUATES_FLAG")
    String degreeGranted;

    @SerializedName(value = "ENROLL_EN")
    String enroll_EN;

    @SerializedName(value = "ENROLL_AR")
    String enroll_AR;

    public String getStudentName_EN() {
        return studentName_EN;
    }

    public String getStudentCode() {
        return studentCode;
    }

    public String getStudentName_AR() {
        return studentName_AR;
    }

    public String getNationality_EN() {
        return nationality_EN;
    }

    public String getNationality_AR() {
        return nationality_AR;
    }

    public String getNationalID_EN() {
        return nationalID_EN;
    }

    public String getEmail() {
        return email;
    }

    public String getMajor_EN() {
        return major_EN;
    }

    public String getMajor_AR() {
        return major_AR;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public String getFaculty_EN() {
        return faculty_EN;
    }

    public String getFaculty_AR() {
        return faculty_AR;
    }

    public String getDegree_EN() {
        return degree_EN;
    }

    public String getDegree_AR() {
        return degree_AR;
    }

    public String getLevel_AR() {
        return level_AR;
    }

    public String getLevel_EN() {
        return level_EN;
    }

    public String getSemesterCH() {
        return semesterCH;
    }

    public String getSemesterGpa() {
        return semesterGpa;
    }

    public String getAcumlateGpa() {
        return acumlateGpa;
    }

    public String getAcumlatePoint() {
        return acumlatePoint;
    }

    public String getAcumlateCredit() {
        return acumlateCredit;
    }

    public String getDegreeGranted() {
        return degreeGranted;
    }

    public String getEnroll_EN() {
        return enroll_EN;
    }

    public String getEnroll_AR() {
        return enroll_AR;
    }
}
