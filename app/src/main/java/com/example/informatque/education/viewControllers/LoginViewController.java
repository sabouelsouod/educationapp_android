package com.example.informatque.education.viewControllers;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.informatque.education.R;
import com.example.informatque.education.helpers.ParentActivity;
import com.example.informatque.education.helpers.Updatable;
import com.example.informatque.education.helpers.retrofit.WebServices;
import com.example.informatque.education.utils.Constants;
import com.example.informatque.education.utils.MySharedPreferences;
import com.example.informatque.education.utils.Utility;
import com.example.informatque.education.viewModels.CourseCatalogViewModel;
import com.example.informatque.education.viewModels.LoginViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class LoginViewController extends ParentActivity implements Updatable, CompoundButton.OnCheckedChangeListener {

    //regionBindViews

    @BindView(R.id.activity_login)
    LinearLayout linearLayout;

    @BindView(R.id.et_UserName)
    EditText et_UserName;

    @BindView(R.id.et_Password)
    EditText et_Password;

    @BindView(R.id.btn_Login)
    Button btn_Login;

    @BindView(R.id.rememberMeCheck)
    CheckBox rememberMeCheck;

    @BindView(R.id.tv_ForgetPassword)
    TextView tv_ForgetPassword;

    //endregion

    String Language;

    @OnClick(R.id.btn_Login)
    void submit() {

        if (!(Utility.isNetworkAvailable(this))) {
            Snackbar.make(linearLayout, R.string.network_error, Snackbar.LENGTH_SHORT).show();
        } else if (et_UserName.getText().toString().equals("") && et_Password.getText().toString().equals("")) {
            Snackbar.make(linearLayout, R.string.empty_field, Snackbar.LENGTH_SHORT).show();
        } else if (et_Password.getText().toString().equals("")) {
            Snackbar.make(linearLayout, R.string.empty_password, Snackbar.LENGTH_SHORT).show();
        } else if (et_UserName.getText().toString().equals("")) {
            Snackbar.make(linearLayout, R.string.empty_user, Snackbar.LENGTH_SHORT).show();
        } else {

            showProgressDialog();

            LoginViewModel.getInstance().login(et_UserName.getText().toString(), et_Password.getText().toString(), LoginViewController.this, LoginViewController.this);


        }
    }

    MySharedPreferences preferences;

    @Override
    public void onBackPressed() {

        preferences.addData("logged", false);

        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);

        preferences = new MySharedPreferences(this);
        Language = preferences.getData(Constants.LANGUAGE_CODE);

//        tv_ForgetPassword.setOnClickListener(this);
        rememberMeCheck.setOnCheckedChangeListener(this);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected int getTitleResource() {
        return 0;
    }

    @Override
    protected boolean isEnableTitle() {
        return false;
    }


    @Override
    public void onClick(View view) {


    }


    @Override
    protected boolean isEnableHome() {
        return false;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if (isChecked) {
            preferences.addData("rememberMe", true);
        } else {
            preferences.addData("rememberMe", false);
            preferences.addData(Constants.USERNAME_LOGIN, "");
        }
    }

    //
    @Override
    public void update(WebServices apiName) {

        if (apiName.equals(WebServices.Login)) {

            hideProgressDialog();

            preferences.addData(Constants.et_UserName, et_UserName.getText().toString());
            preferences.addData(Constants.et_Password, et_Password.getText().toString());
            if (Language.equals(Constants.ENGLISH_CODE)) {

                preferences.addData(Constants.STUDENT_NAME_ENGLISH, LoginViewModel.getInstance().getUser().getStudentName_EN());

            } else {

                preferences.addData(Constants.STUDENT_NAME_ARABIC, LoginViewModel.getInstance().getUser().getStudentName_AR());

            }

            if (rememberMeCheck.isChecked()) {

                preferences.addData("logged", true);
                preferences.addData("rememberMe",true);

            } else {

                preferences.addData("logged", true);
                preferences.addData("rememberMe",false);


            }


            if (LoginViewModel.getInstance().getResult().equalsIgnoreCase("Ok")) {

                Bundle extras = getIntent().getExtras();
                if (extras != null) {

                    String value = extras.getString("Flag");

                    if (value.trim().equals("studentdata")) {

                        startActivity(new Intent(getBaseContext(), StudetDataViewController.class));


                    } else if (value.trim().equals("schoolrecord")) {

                        startActivity(new Intent(getBaseContext(), SchoolRecordViewController.class));

                    } else if (value.trim().equals("course")) {

                        startActivity(new Intent(getBaseContext(), CourseCatalogViewModel.class));

                    } else if (value.trim().equals("Schedule")) {

                        startActivity(new Intent(getBaseContext(), SchoolScheduleViewController.class));

                    }


                    Toast.makeText(LoginViewController.this, "LoginSucess", Toast.LENGTH_SHORT).show();
                } else {

                    Snackbar.make(linearLayout, R.string.wrong_user_password, Snackbar.LENGTH_SHORT).show();

                }

            }
        }
    }

    @Override
    public void updateRefresh(WebServices method) {

    }

}


