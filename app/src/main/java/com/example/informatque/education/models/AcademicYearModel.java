
package com.example.informatque.education.models;

import java.util.List;

import com.example.informatque.education.viewModels.SchoolRecordViewModel;
import com.google.gson.annotations.SerializedName;

public class AcademicYearModel extends SchoolRecordViewModel {


    @SerializedName("ACAD_YEAR_DESCR_AR")
    private String mACADYEARDESCRAR;
    @SerializedName("ACAD_YEAR_DESCR_EN")
    private String mACADYEARDESCREN;
    @SerializedName("ED_ACAD_YEAR_ID")
    private String mEDACADYEARID;
    @SerializedName("Semesters")
    private List<SemesterModel> mSemesterModels;

    public String getACADYEARDESCRAR() {
        return mACADYEARDESCRAR;
    }

    public void setACADYEARDESCRAR(String ACADYEARDESCRAR) {
        mACADYEARDESCRAR = ACADYEARDESCRAR;
    }

    public String getACADYEARDESCREN() {
        return mACADYEARDESCREN;
    }

    public void setACADYEARDESCREN(String ACADYEARDESCREN) {
        mACADYEARDESCREN = ACADYEARDESCREN;
    }

    public String getEDACADYEARID() {
        return mEDACADYEARID;
    }

    public void setEDACADYEARID(String EDACADYEARID) {
        mEDACADYEARID = EDACADYEARID;
    }

    public List<SemesterModel> getSemesters() {
        return mSemesterModels;
    }

    public void setSemesters(List<SemesterModel> semesterModels) {
        mSemesterModels = semesterModels;
    }

}
