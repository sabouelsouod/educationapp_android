package com.example.informatque.education.helpers;

import com.example.informatque.education.helpers.retrofit.WebServices;

/**
 * Created by ibrashraf on 11/14/2017.
 */

public interface Updatable {

    void update(WebServices apiName);

    void updateRefresh(WebServices method);


}
