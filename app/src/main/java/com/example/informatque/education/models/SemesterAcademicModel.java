
package com.example.informatque.education.models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class SemesterAcademicModel {

    @SerializedName("ED_STUD_SEMESTER_ID")
    private String mEDSTUDSEMESTERID;
    @SerializedName("SEMESTER_DESCR_AR")
    private String mSEMESTERDESCRAR;
    @SerializedName("SEMESTER_DESCR_EN")
    private String mSEMESTERDESCREN;
    @SerializedName("Transcripts")
    private Object mTranscripts;

    public String getEDSTUDSEMESTERID() {
        return mEDSTUDSEMESTERID;
    }

    public void setEDSTUDSEMESTERID(String EDSTUDSEMESTERID) {
        mEDSTUDSEMESTERID = EDSTUDSEMESTERID;
    }

    public String getSEMESTERDESCRAR() {
        return mSEMESTERDESCRAR;
    }

    public void setSEMESTERDESCRAR(String SEMESTERDESCRAR) {
        mSEMESTERDESCRAR = SEMESTERDESCRAR;
    }

    public String getSEMESTERDESCREN() {
        return mSEMESTERDESCREN;
    }

    public void setSEMESTERDESCREN(String SEMESTERDESCREN) {
        mSEMESTERDESCREN = SEMESTERDESCREN;
    }

    public Object getTranscripts() {
        return mTranscripts;
    }

    public void setTranscripts(Object Transcripts) {
        mTranscripts = Transcripts;
    }

}
