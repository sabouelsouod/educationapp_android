
package com.example.informatque.education.models;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CourseCatalogModel {

    @SerializedName("CourseTypes")
    private List<CourseTypeModel> mCourseTypeModels;
    @SerializedName("details")
    private String mDetails;
    @SerializedName("Faculties")
    private List<FacultyModel> mFaculties;
    @SerializedName("result")
    private String mResult;

    public List<CourseTypeModel> getCourseTypes() {
        return mCourseTypeModels;
    }

    public void setCourseTypes(List<CourseTypeModel> courseTypeModels) {
        mCourseTypeModels = courseTypeModels;
    }

    public String getDetails() {
        return mDetails;
    }

    public void setDetails(String details) {
        mDetails = details;
    }

    public List<FacultyModel> getFaculties() {
        return mFaculties;
    }

    public void setFaculties(List<FacultyModel> Faculties) {
        mFaculties = Faculties;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
