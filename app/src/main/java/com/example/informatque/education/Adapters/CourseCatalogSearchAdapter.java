package com.example.informatque.education.Adapters;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.informatque.education.R;
import com.example.informatque.education.models.CourseCatalogSearchModel;
import com.example.informatque.education.utils.Constants;
import com.example.informatque.education.utils.MySharedPreferences;
import com.example.informatque.education.utils.Utility;

import java.util.ArrayList;


public class CourseCatalogSearchAdapter extends RecyclerView.Adapter<CourseCatalogSearchAdapter.VHolder> {

    Context context;
    ArrayList<CourseCatalogSearchModel> CourseCatalogSearchlist;
    String languageCode;
    MySharedPreferences preferences;


    public CourseCatalogSearchAdapter(Context context, ArrayList<CourseCatalogSearchModel> CourseCatalogSearchlist) {

        this.context = context;
        this.CourseCatalogSearchlist = CourseCatalogSearchlist;
    }


    @Override
    public VHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.single_course_catalog_view, parent, false);
        return new VHolder(view);
    }

    @Override
    public void onBindViewHolder(VHolder holder, int position) {

        CourseCatalogSearchModel SingleCourse = CourseCatalogSearchlist.get(position);
        preferences = new MySharedPreferences(context);
        languageCode = preferences.getData(Constants.LANGUAGE_CODE);


        if (preferences.getData(Constants.LANGUAGE_CODE).equals(Constants.ENGLISH_CODE)) {

            holder.btncourseCatalogData.setText(SingleCourse.getCOURSEDESCREN());

        } else {

            holder.btncourseCatalogData.setText(SingleCourse.getCOURSEDESCRAR());

        }


        holder.courseCode.setText(SingleCourse.getCOURSECODE());
        holder.creditHours.setText(SingleCourse.getCREDITHOURS());
        holder.languageAr.setText(SingleCourse.getCOURSEDESCRAR());
        holder.languageEn.setText(SingleCourse.getCOURSEDESCREN());
        holder.courseContent.setText(SingleCourse.getCOURSECONTENTSAR());


    }


    @Override
    public int getItemCount() {
        if (CourseCatalogSearchlist != null && CourseCatalogSearchlist.size() > 0)
            return CourseCatalogSearchlist.size();
        else return 0;
    }

    class VHolder extends RecyclerView.ViewHolder {

        TextView courseCode;
        TextView creditHours;
        TextView languageAr;
        TextView languageEn;
        TextView courseContent;

        Button btncourseCatalogData;
        CardView courseCatalogDataCard;


        public VHolder(View itemView) {

            super(itemView);
            courseCode = itemView.findViewById(R.id.Course_code);
            creditHours = itemView.findViewById(R.id.Credit_hours);
            languageAr = itemView.findViewById(R.id.Language_ar);
            languageEn = itemView.findViewById(R.id.language_en);
            courseContent = itemView.findViewById(R.id.course_content);

            btncourseCatalogData = itemView.findViewById(R.id.btn_courseCatalogData);
            courseCatalogDataCard = itemView.findViewById(R.id.courseCatalogDataCard);


            btncourseCatalogData.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (courseCatalogDataCard.getVisibility() == View.VISIBLE) {
                        courseCatalogDataCard.setVisibility(View.GONE);
                        Utility.settingArrowDrawable(btncourseCatalogData, languageCode, context, R.drawable.down);
                    } else {
                        courseCatalogDataCard.setVisibility(View.VISIBLE);
                        //YoYo.with(Techniques.FadeInUp).playOn(studentAcademicDataCard);
                        Utility.settingArrowDrawable(btncourseCatalogData, languageCode, context, R.drawable.up);
                    }
                }
            });

        }
    }
}
