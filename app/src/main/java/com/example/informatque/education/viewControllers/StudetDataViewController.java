package com.example.informatque.education.viewControllers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.informatque.education.R;
import com.example.informatque.education.helpers.ParentActivity;
import com.example.informatque.education.helpers.Updatable;
import com.example.informatque.education.helpers.retrofit.WebServices;
import com.example.informatque.education.utils.Constants;
import com.example.informatque.education.utils.MySharedPreferences;
import com.example.informatque.education.utils.Utility;
import com.example.informatque.education.viewModels.LoginViewModel;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class StudetDataViewController extends ParentActivity implements Updatable {


    //regionBindViews
    @BindView(R.id.tv_StudentCodeValue)
    TextView tv_StudentCodeVale;

    @BindView(R.id.tv_StudentNameValue)
    TextView tv_StudentNameValue;

    @BindView(R.id.tv_NationalityValue)
    TextView tv_NationalityValue;

    @BindView(R.id.tv_NationalIDValue)
    TextView tv_NationalIDValue;

    @BindView(R.id.tv_EmailValue)
    TextView tv_EmailValue;

    @BindView(R.id.tv_MobileNumberValue)
    TextView tv_MobileNumberValue;

    @BindView(R.id.tv_StudentCollageValue)
    TextView tv_StudentCollageValue;

    @BindView(R.id.tv_StudentDegreeValue)
    TextView tv_StudentDegreeValue;

    @BindView(R.id.tv_MajorValue)
    TextView tv_MajorValue;

    @BindView(R.id.tv_LevelValue)
    TextView tv_LevelValue;

    @BindView(R.id.tv_SemesterCreditHourValue)
    TextView tv_SemesterCreditHourValue;

    @BindView(R.id.tv_FullFieldCreditHourValue)
    TextView tv_FullFieldCreditHourValue;

    @BindView(R.id.tv_CumulativeGPAValue)
    TextView tv_CumulativeGPAValue;

    @BindView(R.id.tv_SemesterGPAValue)
    TextView tv_SemesterGPAValue;

    @BindView(R.id.tv_EnrollmentStatusValue)
    TextView tv_EnrollmentStatusValue;

    @BindView(R.id.tv_DegreeGrantedValue)
    TextView tv_DegreeGrantedValue;

    @BindView(R.id.studentAcademicDataCard)
    CardView studentAcademicDataCard;

    @BindView(R.id.btn_AcademicData)
    Button btn_AcademicData;


    String languageCode;

    //endregion

    MySharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);

        preferences = new MySharedPreferences(this);
        languageCode = preferences.getData(Constants.LANGUAGE_CODE);

        if (preferences.getBoolean("logged")) {

            filldata();

        } else {


            LoginViewModel.getInstance().login(preferences.getData(Constants.et_UserName), preferences.getData(Constants.et_Password), StudetDataViewController.this, StudetDataViewController.this);
            filldata();
        }


        btn_AcademicData.setOnClickListener(this);
    }

    void filldata() {

        tv_StudentCodeVale.setText(LoginViewModel.getInstance().getUser().getStudentCode());
        tv_NationalIDValue.setText(LoginViewModel.getInstance().getUser().getNationalID_EN());
        tv_EmailValue.setText(LoginViewModel.getInstance().getUser().getEmail());
        tv_MobileNumberValue.setText(LoginViewModel.getInstance().getUser().getPhoneNo());

        if (languageCode.equals(Constants.ENGLISH_CODE)) {

            tv_StudentNameValue.setText(LoginViewModel.getInstance().getUser().getStudentName_EN());
            tv_NationalityValue.setText(LoginViewModel.getInstance().getUser().getNationality_EN());

            //Academic Data
            tv_StudentCollageValue.setText(LoginViewModel.getInstance().getUser().getFaculty_EN());
            tv_StudentDegreeValue.setText(LoginViewModel.getInstance().getUser().getDegree_EN());
            tv_MajorValue.setText(LoginViewModel.getInstance().getUser().getMajor_EN());
            tv_LevelValue.setText(LoginViewModel.getInstance().getUser().getLevel_EN());
            tv_EnrollmentStatusValue.setText(LoginViewModel.getInstance().getUser().getEnroll_EN());


        } else {

            tv_StudentNameValue.setText(LoginViewModel.getInstance().getUser().getStudentName_AR());
            tv_NationalityValue.setText(LoginViewModel.getInstance().getUser().getNationality_AR());

            //Academic Data
            tv_StudentCollageValue.setText(LoginViewModel.getInstance().getUser().getFaculty_AR());
            tv_StudentDegreeValue.setText(LoginViewModel.getInstance().getUser().getDegree_AR());
            tv_MajorValue.setText(LoginViewModel.getInstance().getUser().getMajor_AR());
            tv_LevelValue.setText(LoginViewModel.getInstance().getUser().getLevel_AR());
            tv_EnrollmentStatusValue.setText(LoginViewModel.getInstance().getUser().getEnroll_AR());

        }


        //Academic Data
        tv_SemesterCreditHourValue.setText(LoginViewModel.getInstance().getUser().getSemesterCH());
        tv_SemesterGPAValue.setText(LoginViewModel.getInstance().getUser().getSemesterGpa());

        tv_FullFieldCreditHourValue.setText(LoginViewModel.getInstance().getUser().getAcumlateCredit());
        tv_CumulativeGPAValue.setText(LoginViewModel.getInstance().getUser().getAcumlateGpa());

        if (preferences.getInt(Constants.DEGREE_GRANTED) == 0)
            tv_DegreeGrantedValue.setText(getResources().getString(R.string.textView_text_studentDegreeGrantedValue_no));
        else
            tv_DegreeGrantedValue.setText(getResources().getString(R.string.textView_text_studentDegreeGrantedValue_yes));


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_studet_data_view_controller;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean isEnableHome() {
        return false;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected int getTitleResource() {
        return R.string.textView_text_studentData_title;
    }

    @Override
    protected boolean isEnableTitle() {
        return false;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_AcademicData:
                if (studentAcademicDataCard.getVisibility() == View.VISIBLE) {
                    studentAcademicDataCard.setVisibility(View.GONE);
                    Utility.settingArrowDrawable(btn_AcademicData, languageCode, activity, R.drawable.down);
                } else {
                    studentAcademicDataCard.setVisibility(View.VISIBLE);
                    //YoYo.with(Techniques.FadeInUp).playOn(studentAcademicDataCard);
                    Utility.settingArrowDrawable(btn_AcademicData, languageCode, activity, R.drawable.up);
                }

                break;
        }

    }


    @Override
    public void onBackPressed() {
        Intent homeIntent = new Intent(this, HomeViewComtroller.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, HomeViewComtroller.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return (super.onOptionsItemSelected(menuItem));

    }


    @Override
    public void update(WebServices apiName) {

//        filldata();
    }

    @Override
    public void updateRefresh(WebServices method) {

    }
}
