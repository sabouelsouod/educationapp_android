package com.example.informatque.education.viewControllers;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.informatque.education.R;
import com.example.informatque.education.helpers.ParentActivity;
import com.example.informatque.education.helpers.Updatable;
import com.example.informatque.education.helpers.retrofit.WebServices;
import com.example.informatque.education.models.SemesterModel;
import com.example.informatque.education.models.TranscriptModel;
import com.example.informatque.education.utils.Constants;
import com.example.informatque.education.utils.MySharedPreferences;
import com.example.informatque.education.utils.Utility;
import com.example.informatque.education.viewModels.LoginViewModel;
import com.example.informatque.education.viewModels.SchoolRecordViewModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SchoolRecordViewController extends ParentActivity implements Updatable {


    @BindView(R.id.activity_school_record)
    LinearLayout linearLayout;

    @BindView(R.id.sp_UniversityYears)
    Spinner spUniversityYears;


    @BindView(R.id.semesterContainerLayout)
    LinearLayout semesterContainerLayout;

    String languageCode;
    MySharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);

        preferences = new MySharedPreferences(this);

        languageCode = preferences.getData(Constants.LANGUAGE_CODE);

        showProgressDialog();

        if (SchoolRecordViewModel.getInstance().getAcademicYearModels() == null) {
            String StudId = LoginViewModel.getInstance().getUserData().getStudentID();
            String AsFacultyInfo = LoginViewModel.getInstance().getUserData().getFacultyInfo();
            String SemesterId = LoginViewModel.getInstance().getUserData().getSemesterCode();
            String YearId = LoginViewModel.getInstance().getUserData().getAcademicYear();
            SchoolRecordViewModel.getInstance().getDAta(StudId, AsFacultyInfo, YearId, SemesterId, SchoolRecordViewController.this, SchoolRecordViewController.this);

        } else {

            initializeAcademicYearsSpinner();
            hideProgressDialog();
        }

    }

    @Override
    protected int getLayoutResource() {

        return R.layout.activity_school_record;
    }

    @Override
    protected boolean isEnableTitle() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected int getTitleResource() {
        return R.string.textView_text_schoolRecord_title;
    }

    @Override
    public void onClick(View view) {

    }


    void initializeAcademicYearsSpinner() {

        final String[] academicYearsList = new String[Integer.parseInt(String.valueOf(SchoolRecordViewModel.getInstance().getAcademicYearModels().size()))];
        for (int k = 0; k < SchoolRecordViewModel.getInstance().getAcademicYearModels().size(); k++) {

            if (preferences.getData(Constants.LANGUAGE_CODE).equals(Constants.ARABIC_CODE)) {

                academicYearsList[k] = SchoolRecordViewModel.getInstance().getAcademicYearModels().get(k).getACADYEARDESCREN();

            } else {

                academicYearsList[k] = SchoolRecordViewModel.getInstance().getAcademicYearModels().get(k).getACADYEARDESCREN();

            }
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getBaseContext(), R.layout.spinner_background, academicYearsList);
        dataAdapter.setDropDownViewResource(R.layout.spinner_item);
        spUniversityYears.setAdapter(dataAdapter);
        spUniversityYears.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {

                //To clear the previous Views
                semesterContainerLayout.removeAllViews();

                ArrayList<SemesterModel> semesters = (ArrayList) SchoolRecordViewModel.getInstance().getAcademicYearModels().get(position).getSemesters();


                for (int i = 0; i < semesters.size(); i++) {

                    //SemesterModel Button
                    View buttonView = LayoutInflater.from(getBaseContext()).inflate(R.layout.semester_button, parent, false);
                    final Button button = buttonView.findViewById(R.id.btn_Semester);

                    View cardView = LayoutInflater.from(getBaseContext()).inflate(R.layout.courses_card, parent, false);
                    final CardView coursesCard = cardView.findViewById(R.id.coursesCard);
                    LinearLayout coursesList = cardView.findViewById(R.id.coursesList);

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(0, 0, 0, 20);
                    cardView.setLayoutParams(params);

                    coursesCard.setBackgroundColor(ContextCompat.getColor(getBaseContext(), R.color.White));
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.setMargins(20, 0, 20, 0);

                    if (preferences.getData(Constants.LANGUAGE_CODE).equals(Constants.ENGLISH_CODE)) {

                        button.setText(semesters.get(i).getSEMESTERDESCREN());
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            button.setElevation(8);
                        }
                    } else {

                        button.setText(semesters.get(i).getSEMESTERDESCRAR());
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            button.setElevation(8);
                        }
                    }


                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (coursesCard.getVisibility() == View.VISIBLE) {
                                coursesCard.setVisibility(View.GONE);
                                Utility.settingArrowDrawable(button, preferences.getData(Constants.LANGUAGE_CODE), getBaseContext(), R.drawable.down);
                            } else {
                                //Load animation
                                /*Animation slide_down = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
                                coursesCard.startAnimation(slide_down);*/
                                coursesCard.setVisibility(View.VISIBLE);
                                Utility.settingArrowDrawable(button, preferences.getData(Constants.LANGUAGE_CODE), getBaseContext(), R.drawable.up);
                            }
                        }
                    });
                    final ArrayList<TranscriptModel> transcriptModels = (ArrayList) SchoolRecordViewModel.getInstance().getAcademicYearModels().get(position).getSemesters().get(i).getTranscripts();
                    for (int j = 0; j < transcriptModels.size(); j++) {

                        View courseRow = LayoutInflater.from(getBaseContext()).inflate(R.layout.course_row, parent, false);
                        final TextView tv_CourseCodeValue = courseRow.findViewById(R.id.tv_CourseCodeValue);
                        final TextView tv_CourseDescriptionValue = courseRow.findViewById(R.id.tv_CourseDescriptionValue);

                        final int TranscriptModelIndex = j;

                        //Add Listener to the Single Course
                        courseRow.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent intent = new Intent(getBaseContext(), CourseDetailsViewController.class);
                                Bundle bundle = new Bundle();

                                bundle.putString(Constants.COURSE_CODE, transcriptModels.get(TranscriptModelIndex).getCOURSECODE());

                                if (preferences.getData(Constants.LANGUAGE_CODE).equals(Constants.ENGLISH_CODE)) {
                                    bundle.putString(Constants.COURSE_DESCRIPTION_ARABIC, transcriptModels.get(TranscriptModelIndex).getCOURSEDESCREN());
                                    bundle.putString(Constants.COURSE_GRADE, transcriptModels.get(TranscriptModelIndex).getGRADINGEN());
                                    bundle.putString(Constants.COURSE_STATUS, transcriptModels.get(TranscriptModelIndex).getSymbolEN());

                                } else {

                                    bundle.putString(Constants.COURSE_DESCRIPTION_ARABIC, transcriptModels.get(TranscriptModelIndex).getCOURSEDESCRAR());
                                    bundle.putString(Constants.COURSE_GRADE, transcriptModels.get(TranscriptModelIndex).getGRADINGAR());
                                    bundle.putString(Constants.COURSE_STATUS, transcriptModels.get(TranscriptModelIndex).getSymbolAR());

                                }

                                bundle.putDouble(Constants.COURSE_CREDIT_HOUR, Double.parseDouble(transcriptModels.get(TranscriptModelIndex).getCREDITHOURS()));

                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        });


                        if (preferences.getData(Constants.LANGUAGE_CODE).equals(Constants.ENGLISH_CODE)) {
                            tv_CourseDescriptionValue.setText(transcriptModels.get(TranscriptModelIndex).getCOURSEDESCREN());

                        } else {

                            tv_CourseDescriptionValue.setText(transcriptModels.get(TranscriptModelIndex).getCOURSEDESCRAR());

                        }


                        tv_CourseCodeValue.setText(transcriptModels.get(TranscriptModelIndex).getCOURSECODE());


                        coursesList.addView(courseRow, layoutParams);
                    }

//                    //SemesterModel Summary
                    View semesterSummary = LayoutInflater.from(getBaseContext()).inflate(R.layout.semester_summary, parent, false);
                    TextView tv_EnrollmentStatusValue = semesterSummary.findViewById(R.id.tv_EnrollmentStatusValue);
                    TextView tv_SemesterGPAValue = semesterSummary.findViewById(R.id.tv_SemesterGPAValue);
                    TextView tv_SemesterTotalCreditHourValue = semesterSummary.findViewById(R.id.tv_SemesterTotalCreditHourValue);
                    TextView tv_SemesterPointsValue = semesterSummary.findViewById(R.id.tv_SemesterPointsValue);
                    TextView tv_CumulativeGPAValue = semesterSummary.findViewById(R.id.tv_CumulativeGPAValue);
                    TextView tv_CumulativeCreditHoursValue = semesterSummary.findViewById(R.id.tv_CumulativeCreditHoursValue);
                    TextView tv_CumulativePointsValue = semesterSummary.findViewById(R.id.tv_CumulativePointsValue);
                    TextView tv_MajorGPAValue = semesterSummary.findViewById(R.id.tv_MajorGPAValue);


                    if (preferences.getData(Constants.LANGUAGE_CODE).equals(Constants.ENGLISH_CODE)) {

                        tv_EnrollmentStatusValue.setText(transcriptModels.get(0).getENROLLEN());

                    } else {

                        tv_EnrollmentStatusValue.setText(transcriptModels.get(0).getENROLLEN());

                    }


                    tv_SemesterGPAValue.setText(String.valueOf(transcriptModels.get(0).getSEMGPA()));
                    tv_SemesterTotalCreditHourValue.setText(transcriptModels.get(0).getSEMCH());
                    tv_SemesterPointsValue.setText(String.valueOf(transcriptModels.get(0).getSEMPOINT()));
                    tv_CumulativeGPAValue.setText(String.valueOf(transcriptModels.get(0).getACCUMGPA()));
                    tv_CumulativeCreditHoursValue.setText(String.valueOf(transcriptModels.get(0).getACCUMCH()));
                    tv_CumulativePointsValue.setText(String.valueOf(transcriptModels.get(0).getACCUMPOINT()));
                    tv_MajorGPAValue.setText(String.valueOf(transcriptModels.get(0).getMAJORCGPA()));

                    coursesList.addView(semesterSummary);

                    semesterContainerLayout.addView(buttonView);
                    semesterContainerLayout.addView(cardView);

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    protected boolean isEnableHome() {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, HomeViewComtroller.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return (super.onOptionsItemSelected(menuItem));

    }


    @Override
    public void onBackPressed() {
        Intent homeIntent = new Intent(this, HomeViewComtroller.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }

    @Override
    public void update(WebServices apiName) {
        if (apiName.equals(WebServices.SchoolRecords)) {
            initializeAcademicYearsSpinner();
            hideProgressDialog();
        }


    }

    @Override
    public void updateRefresh(WebServices method) {

    }
}
