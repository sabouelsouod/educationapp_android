package com.example.informatque.education.helpers.retrofit;

import com.example.informatque.education.utils.Constants;
import com.example.informatque.education.viewModels.AcademicCalenderViewModel;
import com.example.informatque.education.viewModels.SearchAcademicViewModel;
import com.example.informatque.education.viewModels.SearchCourseCatalogViewModel;
import com.example.informatque.education.viewModels.CourseCatalogViewModel;
import com.example.informatque.education.viewModels.CourseRegestrationViewModel;
import com.example.informatque.education.viewModels.LoginViewModel;
import com.example.informatque.education.viewModels.SchoolRecordViewModel;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface EducationServices {

    @POST(Constants.LOGIN_URL)
    Call<LoginViewModel> loginUser(@Body JsonObject jsonObject);


    @POST(Constants.TRANSCRIPT_URL)
    Call<SchoolRecordViewModel> TranscriptData(@Body JsonObject jsonObject);

    @POST(Constants.Courses_URL)
    Call<CourseRegestrationViewModel> CoursesRegestrationData(@Body JsonObject jsonObject);

    @GET(Constants.DATA_URL_Courses)
    Call<CourseCatalogViewModel> CourseCatalogFilterData();

    @POST(Constants.SEARCH_COURSE_CATALOG)
    Call<SearchCourseCatalogViewModel> COURSE_CATALOG_SEARCH_VIEW_MODEL_CALL(@Body JsonObject jsonObject);

    @GET(Constants.DATA_URL_AcdYears)
    Call<AcademicCalenderViewModel> ACADEMIC_CALENDER_VIEW_MODEL_CALL();

    @POST(Constants.SEARCH_ACADEMIC_CALENDAR)
    Call<SearchAcademicViewModel> SEARCH_ACADEMIC_VIEW_MODEL_CALL(@Body JsonObject jsonObject);


}