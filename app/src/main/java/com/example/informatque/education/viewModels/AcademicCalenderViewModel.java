package com.example.informatque.education.viewModels;

import android.content.Context;
import android.util.Log;

import com.example.informatque.education.EducationAppController;
import com.example.informatque.education.helpers.Updatable;
import com.example.informatque.education.helpers.retrofit.EducationServices;
import com.example.informatque.education.helpers.retrofit.WebServices;
import com.example.informatque.education.models.AcadYearModel;
import com.example.informatque.education.models.FacultyAcademicModel;
import com.example.informatque.education.models.SemesterAcademicModel;
import com.example.informatque.education.utils.Utils;
import com.google.gson.annotations.SerializedName;


import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AcademicCalenderViewModel {

    private static final String TAG = AcademicCalenderViewModel.class.getSimpleName();

    private Updatable updatable;
    public static AcademicCalenderViewModel instance = new AcademicCalenderViewModel();


    @SerializedName("AcadYears")
    private ArrayList<AcadYearModel> mAcadYearModels;
    @SerializedName("details")
    private String mDetails;
    @SerializedName("Faculties")
    private ArrayList<FacultyAcademicModel> mFaculties;
    @SerializedName("result")
    private String mResult;
    @SerializedName("Semester")
    private ArrayList<SemesterAcademicModel> mSemesterAcademicModel;

    public ArrayList<AcadYearModel> getmAcadYearModels() {
        return mAcadYearModels;
    }

    public String getmDetails() {
        return mDetails;
    }

    public ArrayList<FacultyAcademicModel> getmFaculties() {
        return mFaculties;
    }

    public String getmResult() {
        return mResult;
    }

    public ArrayList<SemesterAcademicModel> getmSemesterAcademicModel() {
        return mSemesterAcademicModel;
    }

    public Context context;


    public static AcademicCalenderViewModel getInstance() {
        if (instance == null)
            instance = new AcademicCalenderViewModel();
        return instance;
    }

    /*---------------------------------------------------*/
    public void getData(Context context, final Updatable updatable) {
        this.updatable = updatable;
        this.context = context;


        EducationAppController application = EducationAppController.get(context);
        EducationServices educationServices = application.getJMTServices();


        Call<AcademicCalenderViewModel> call = educationServices.ACADEMIC_CALENDER_VIEW_MODEL_CALL();
        call.enqueue(new Callback<AcademicCalenderViewModel>() {
            @Override
            public void onResponse(Call<AcademicCalenderViewModel> call, Response<AcademicCalenderViewModel> response) {
                onSuccess(response);
            }

            @Override
            public void onFailure(Call<AcademicCalenderViewModel> call, Throwable t) {
                onError();
                Log.e(TAG, "onFailure");
                Log.e(TAG, "Throwable" + t.getMessage());
            }

        });


    }


    /*---------------------------------------------------*/
    private void onSuccess(Response<AcademicCalenderViewModel> response) {
        // TODO: 11/15/2017 Mapping Response Data To View Model Singleton object
        instance = response.body();
        if (instance != null) {
            updatable.update(WebServices.AcademicCalender);
        }
    }

    private void onError() {
        Utils.getInstance(context).dismissProgress();
    }

}
