package com.example.informatque.education.viewModels;

import android.content.Context;
import android.databinding.BaseObservable;
import android.util.Log;

import com.example.informatque.education.EducationAppController;
import com.example.informatque.education.helpers.Updatable;
import com.example.informatque.education.helpers.retrofit.EducationServices;
import com.example.informatque.education.helpers.retrofit.WebServices;
import com.example.informatque.education.models.UserDataModel;
import com.example.informatque.education.models.UserModel;
import com.example.informatque.education.utils.Utils;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginViewModel  {


    private static final String TAG = LoginViewModel.class.getSimpleName();

    @SerializedName(value = "result")
    String result;

    @SerializedName(value = "details")
    String details;

    @SerializedName(value = "CardProfile")
    UserModel user;

    @SerializedName(value = "UserMainData")
    UserDataModel userData;

    public String getResult() {
        return result;
    }

    public String getDetails() {
        return details;
    }

    public UserModel getUser() {
        return user;
    }

    public UserDataModel getUserData() {
        return userData;
    }

    private Updatable updatable;
    public static LoginViewModel instance = new LoginViewModel();

    public Context context;

    public static LoginViewModel getInstance() {
        if (instance == null)
            instance = new LoginViewModel();
        return instance;
    }

    /*---------------------------------------------------*/
    public void login(String username, String password, Context context, final Updatable updatable) {
        this.updatable = updatable;
        this.context = context;


        EducationAppController application = EducationAppController.get(context);
        EducationServices educationServices = application.getJMTServices();


        JsonObject obj = new JsonObject();
        obj.addProperty("UserName", username);
        obj.addProperty("Password", password);

        Log.i("Boady", obj.toString());


        Call<LoginViewModel> call = educationServices.loginUser(obj);
        call.enqueue(new Callback<LoginViewModel>() {
            @Override
            public void onResponse(Call<LoginViewModel> call, Response<LoginViewModel> response) {
                onSuccess(response);
            }

            @Override
            public void onFailure(Call<LoginViewModel> call, Throwable t) {
                onError();
                Log.e(TAG, "onFailure");
                Log.e(TAG, "Throwable" + t.getMessage());
            }

        });


    }


    public void UserData(Context context, final Updatable updatable) {
        this.updatable = updatable;
        this.context = context;

    }


    /*---------------------------------------------------*/
    private void onSuccess(Response<LoginViewModel> response) {
        // TODO: 11/15/2017 Mapping Response Data To View Model Singleton object
        instance = response.body();
        if (instance != null) {
            updatable.update(WebServices.Login);
        }
    }


    private void onError() {
        Utils.getInstance(context).dismissProgress();
    }


}
