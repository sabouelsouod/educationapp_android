package com.example.informatque.education.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.util.Patterns;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import android.widget.Toast;

import com.example.informatque.education.R;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.Set;


public class Utils {
    private static final String TAG = Utils.class.getSimpleName();
    public static Utils instance;
    Context context;
    private ProgressDialog progressDialog;


    public Utils(Context context) {
        this.context = context;
        progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(context.getResources().getString(R.string.progressDialog_message));
    }

    public static Utils getInstance(Context context) {
        if (instance == null) {
            instance = new Utils(context);
        }
        return instance;
    }

    public static void displayNextActivityFinish(Activity currentActivity, Class<?> nextActivityClass) {
        Intent i = new Intent(currentActivity, nextActivityClass);
        currentActivity.startActivity(i);
        currentActivity.overridePendingTransition(0, 0);
        currentActivity.finish();
    }


    public static void displayNextActivity(Activity currentActivity, Class<?> nextActivityClass) {
        Intent i = new Intent(currentActivity, nextActivityClass);
        currentActivity.startActivity(i);
        currentActivity.overridePendingTransition(0, 0);
    }


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static boolean isValidUrl(String urlString) {
        try {
            URL url = new URL(urlString);
            return URLUtil.isValidUrl(urlString) && Patterns.WEB_URL.matcher(urlString).matches();
        } catch (MalformedURLException e) {

        }
        return false;
    }

    public static void openLink(Context context, String link) {
        Uri webpage = Uri.parse(link);

        if (!link.startsWith("http://") && !link.startsWith("https://")) {
            webpage = Uri.parse("https://" + link);
        }

        link = webpage.toString();

        Log.d("nahmed", link);

        if (Utils.isValidUrl(link)) {
            Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
            context.startActivity(intent);
        } else {
            Toast.makeText(context, "This is not valid url", Toast.LENGTH_LONG).show();
        }
    }


    /*//*
     * A function that colors a string
     */
    public static SpannableStringBuilder setTextColor(String textToBeColored, int color) {
        final SpannableStringBuilder sb = new SpannableStringBuilder(textToBeColored);

        final ForegroundColorSpan fcs = new ForegroundColorSpan(color);

        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

        sb.setSpan(fcs, 0, textToBeColored.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        return sb;
    }

    public static Bitmap resizeMapIcons(Context context, int drawable, int width, int height) {
        Bitmap imageBitmap = BitmapFactory.decodeResource(context.getResources(), drawable);
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, width, height, false);
        return resizedBitmap;
    }

    public static JsonArray convertToJsonArray(Set<Integer> list, JsonParser parser, Gson gson) {
        return parser.parse(gson.toJson(list)).getAsJsonArray();
    }

    public static String getFilename() {

        String uriSting = /*(file.getAbsolutePath() + "/"
                + */System.currentTimeMillis() + ".jpg";
        return uriSting;
    }


    public static String saveFile(String path) {

        String folderPath = path.substring(0, path.lastIndexOf("/"));

        File direct = new File(folderPath/*Environment.getExternalStorageDirectory() + "/MyAppFolder/MyApp/"*/);

        String newFile = folderPath + "/" + getFilename();

        File file = new File(newFile);

        if (!direct.exists()) {
            direct.mkdir();
        }

        if (!file.exists()) {
            try {
                file.createNewFile();
                FileChannel src = new FileInputStream(path).getChannel();
                FileChannel dst = new FileOutputStream(file).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return newFile;
    }


    public static long getFolderSize(File f) {
        long size = 0;
        if (f.isDirectory()) {
            for (File file : f.listFiles()) {
                size += getFolderSize(file);
            }
        } else {
            size = f.length();
        }
        return size;
    }

    public static int getExifRotation(String imgPath) {
        try {
            ExifInterface exif = new ExifInterface(imgPath);
            String rotationAmount = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
            if (!TextUtils.isEmpty(rotationAmount)) {
                int rotationParam = Integer.parseInt(rotationAmount);
                switch (rotationParam) {
                    case ExifInterface.ORIENTATION_NORMAL:
                        return 0;
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        return 90;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        return 180;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        return 270;
                    default:
                        return 0;
                }
            } else {
                return 0;
            }
        } catch (Exception ex) {
            return 0;
        }
    }

    public static String saveRotatedImage(Context context, Bitmap bmp, String path) {
        String newPath = "";
        try {
            String folderPath = path.substring(0, path.lastIndexOf("/"));
            newPath = folderPath + "/" + getFilename();
            Uri uri = Uri.parse("file://" + newPath);
            OutputStream os = context.getContentResolver().openOutputStream(uri);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, os);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return newPath;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }


    public void showProgress() {
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void showProgressWithTimer() {

        progressDialog.show();
        progressDialog.setProgress(100);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                progressDialog.dismiss();
            }
        }, 3000);
    }

    public void dismissProgress() {
        if (progressDialog != null) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
            instance = null;
        }
    }

    public static int getScreenSize(Context context) {
        Configuration configuration = context.getResources().getConfiguration();
        return configuration.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
    }

    public static boolean passwordValidation(Context context, String pass) {
        if (pass.length() < 8) {
            Toast.makeText(context, "password is too short, please enter min 8 characters.", Toast.LENGTH_LONG).show();
            return false;
        } else if (!pass.matches(".*\\d.*") && !pass.matches(".*[!@#$%^&*+=?-].*")) {
            Toast.makeText(context, "Please enter at least 1 digit 1 special characters.", Toast.LENGTH_LONG).show();
            return false;
        } else if (!pass.matches(".*[!@#$%^&*+=?-].*")) {
            Toast.makeText(context, "Please enter at least 1 special characters.", Toast.LENGTH_LONG).show();
            return false;
        } else if (!pass.matches(".*[A-Z].*")) {
            Toast.makeText(context, "Please enter at least 1 upper case letter.", Toast.LENGTH_LONG).show();
            return false;
        } else if (!pass.matches(".*[a-z].*")) {
            Toast.makeText(context, "Please enter at least 1 lower case letter.", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

}
