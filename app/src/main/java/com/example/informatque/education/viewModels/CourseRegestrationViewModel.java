package com.example.informatque.education.viewModels;

import android.content.Context;
import android.util.Log;

import com.example.informatque.education.EducationAppController;
import com.example.informatque.education.helpers.Updatable;
import com.example.informatque.education.helpers.retrofit.EducationServices;
import com.example.informatque.education.helpers.retrofit.WebServices;
import com.example.informatque.education.models.CoursesRegestrationModel;
import com.example.informatque.education.utils.Utils;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CourseRegestrationViewModel {

    private static final String TAG = CourseRegestrationViewModel.class.getSimpleName();

    private Updatable updatable;
    public static CourseRegestrationViewModel instance = new CourseRegestrationViewModel();


    @SerializedName(value = "result")
    String result;

    @SerializedName(value = "details")
    String details;

    @SerializedName(value = "items")
    ArrayList<CoursesRegestrationModel> coursesRegestrationModels;

    public Context context;


    public String getResult() {
        return result;
    }

    public String getDetails() {
        return details;
    }

    public ArrayList<CoursesRegestrationModel> getCoursesRegestrationModels() {
        return coursesRegestrationModels;
    }

    public static CourseRegestrationViewModel getInstance() {
        if (instance == null)
            instance = new CourseRegestrationViewModel();
        return instance;
    }

    /*---------------------------------------------------*/
    public void getDAta(String StudId, String EdYearId, String EdCodeSemester, Context context, final Updatable updatable) {
        this.updatable = updatable;
        this.context = context;


        EducationAppController application = EducationAppController.get(context);
        EducationServices educationServices = application.getJMTServices();


        JsonObject obj = new JsonObject();
        obj.addProperty("EdStudId", StudId);
        obj.addProperty("EdAcadYearId", EdYearId);
        obj.addProperty("EdCodeSemesterId", EdCodeSemester);


        Log.i("Boady", obj.toString());


        Call<CourseRegestrationViewModel> call = educationServices.CoursesRegestrationData(obj);
        call.enqueue(new Callback<CourseRegestrationViewModel>() {
            @Override
            public void onResponse(Call<CourseRegestrationViewModel> call, Response<CourseRegestrationViewModel> response) {
                onSuccess(response);
            }

            @Override
            public void onFailure(Call<CourseRegestrationViewModel> call, Throwable t) {
                onError();
                Log.e(TAG, "onFailure");
                Log.e(TAG, "Throwable" + t.getMessage());
            }

        });


    }


    /*---------------------------------------------------*/
    private void onSuccess(Response<CourseRegestrationViewModel> response) {
        // TODO: 11/15/2017 Mapping Response Data To View Model Singleton object
        instance = response.body();
        if (instance != null) {
            updatable.update(WebServices.CourseRegestration);
        }
    }

    private void onError() {
        Utils.getInstance(context).dismissProgress();
    }

}
