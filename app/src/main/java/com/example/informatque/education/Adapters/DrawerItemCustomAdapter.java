package com.example.informatque.education.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.informatque.education.R;
import com.example.informatque.education.models.DataModel;


/**
 * Created by ibrashraf on 9/27/2017.
 */

public class DrawerItemCustomAdapter extends ArrayAdapter<DataModel> {

    Context mContext;
    int layoutResourceId;
    DataModel data[] = null;


    public DrawerItemCustomAdapter(Context mContext, int layoutResourceId, DataModel[] data) {

        super(mContext, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DataModel folder = data[position];


        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();

        View listItem = inflater.inflate(layoutResourceId, parent, false);
        listItem.setBackgroundResource(R.drawable.selector_item_drawer);

        ImageView imageViewIcon = listItem.findViewById(R.id.iv_drawer_icon);


        TextView textViewName = listItem.findViewById(R.id.textViewName);


        imageViewIcon.setImageResource(folder.icon);


        textViewName.setText(folder.name);

        return listItem;
    }
}

