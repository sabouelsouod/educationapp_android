package com.example.informatque.education.viewModels;

import android.content.Context;
import android.util.Log;

import com.example.informatque.education.EducationAppController;
import com.example.informatque.education.helpers.Updatable;
import com.example.informatque.education.helpers.retrofit.EducationServices;
import com.example.informatque.education.helpers.retrofit.WebServices;
import com.example.informatque.education.models.CourseCatalogSearchModel;
import com.example.informatque.education.utils.Utils;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SearchCourseCatalogViewModel {

    private static final String TAG = SearchCourseCatalogViewModel.class.getSimpleName();

    private Updatable updatable;
    public static SearchCourseCatalogViewModel instance = new SearchCourseCatalogViewModel();


    @SerializedName(value = "result")
    String result;

    @SerializedName(value = "details")
    String details;

    @SerializedName(value = "items")
    ArrayList<CourseCatalogSearchModel> CourseCatalogSearchModels;

    public Context context;


    public String getResult() {
        return result;
    }

    public String getDetails() {
        return details;
    }

    public ArrayList<CourseCatalogSearchModel> getCourseCatalogSearchModels() {
        return CourseCatalogSearchModels;
    }

    public static SearchCourseCatalogViewModel getInstance() {
        if (instance == null)
            instance = new SearchCourseCatalogViewModel();
        return instance;
    }

    /*---------------------------------------------------*/
    public void getSearchDAta(int EdCodeCourseId, int AsFacultyInfoId, int DeptId,
                              String CrsName, String CrsCode, int AsCodeDegreeClassId
            , int ActiveFlg, int OnlineFlg,
                              Context context, final Updatable updatable) {
        this.updatable = updatable;
        this.context = context;


        EducationAppController application = EducationAppController.get(context);
        EducationServices educationServices = application.getJMTServices();


        JsonObject obj = new JsonObject();
        obj.addProperty("AsFacultyInfoId", AsFacultyInfoId);
        obj.addProperty("CrsCode", CrsCode);
        obj.addProperty("CrsName", CrsName);
        obj.addProperty("EdCodeCourseId", EdCodeCourseId);
        obj.addProperty("DeptId", DeptId);
        obj.addProperty("AsCodeDegreeClassId", AsCodeDegreeClassId);
        obj.addProperty("OnlineFlg", OnlineFlg);
        obj.addProperty("ActiveFlg", ActiveFlg);


        Log.i("Boady", obj.toString());


        Call<SearchCourseCatalogViewModel> call = educationServices.COURSE_CATALOG_SEARCH_VIEW_MODEL_CALL(obj);
        call.enqueue(new Callback<SearchCourseCatalogViewModel>() {
            @Override
            public void onResponse(Call<SearchCourseCatalogViewModel> call, Response<SearchCourseCatalogViewModel> response) {
                onSuccess(response);
            }

            @Override
            public void onFailure(Call<SearchCourseCatalogViewModel> call, Throwable t) {
                onError();
                Log.e(TAG, "onFailure");
                Log.e(TAG, "Throwable" + t.getMessage());
            }

        });


    }


    /*---------------------------------------------------*/
    private void onSuccess(Response<SearchCourseCatalogViewModel> response) {
        // TODO: 11/15/2017 Mapping Response Data To View Model Singleton object
        instance = response.body();
        if (instance != null) {
            updatable.update(WebServices.CourseCatalogSearch);
        }
    }

    private void onError() {
        Utils.getInstance(context).dismissProgress();
    }

}
