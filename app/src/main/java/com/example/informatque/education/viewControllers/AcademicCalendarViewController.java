package com.example.informatque.education.viewControllers;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.informatque.education.Adapters.AcademicCalenderSearchAdapter;
import com.example.informatque.education.R;
import com.example.informatque.education.helpers.ParentActivity;
import com.example.informatque.education.helpers.Updatable;
import com.example.informatque.education.helpers.retrofit.WebServices;
import com.example.informatque.education.models.SearchAcademicModel;
import com.example.informatque.education.utils.Constants;
import com.example.informatque.education.utils.MySharedPreferences;
import com.example.informatque.education.utils.Utility;
import com.example.informatque.education.viewModels.AcademicCalenderViewModel;
import com.example.informatque.education.viewModels.SearchAcademicViewModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AcademicCalendarViewController extends ParentActivity implements Updatable {

    //regionBindViews

    //regionRecycleView
    @BindView(R.id.recyclerViewAcademicCalendar)
    RecyclerView recyclerView;
    @BindView(R.id.transitions_container)
    LinearLayout linearLayout;
    //endregion

    //regionSpinners
    @BindView(R.id.acd_year_sp)
    Spinner spinnerAcadYear;

    @BindView(R.id.univ_sp)
    Spinner spinnerCollage;

    @BindView(R.id.sp_semester)
    Spinner spinnerSemester;
    //endregion

    @BindView(R.id.btnSearch)
    Button searchBtn;

    @BindView(R.id.academicCalendarRG)
    RadioGroup mainRadioGroup;


    //endregion

    ArrayAdapter<String> spinnerAcadYearsAdapter, spinnerCollagesAdapter, spinnerSemestersAdapter;

    ArrayList<SearchAcademicModel> searchAcademicModelsList = new ArrayList<>();

    MySharedPreferences preferences;

    AcademicCalenderSearchAdapter adapter;

    ProgressDialog mProgressDialog;

    //An ArrayList for Spinner Items
    ArrayList<String> acdYearsList = new ArrayList<>();
    ArrayList<String> CollageList = new ArrayList<>();
    ArrayList<String> SemesterList = new ArrayList<>();


    int EdAcadYear = 0;
    int AsFacultyInfoId = 0;
    int Semester = 0;
    int selectedDegreeRG;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);

        mProgressDialog = new ProgressDialog(AcademicCalendarViewController.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.progressDialog_message));

        AcademicCalenderViewModel.getInstance().getData(AcademicCalendarViewController.this, AcademicCalendarViewController.this);

        initUi();


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_academic_calendar;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected int getTitleResource() {
        return R.string.textView_text_academic_calendar;
    }

    @Override
    protected boolean isEnableTitle() {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.btnSearch:
                if (Utility.isNetworkAvailable(this)) {

                    showProgressDialog();


                    SearchAcademicViewModel.getInstance().getSearchDAta(EdAcadYear, AsFacultyInfoId, Semester, selectedDegreeRG,
                            AcademicCalendarViewController.this, AcademicCalendarViewController.this);


                } else {
                    Snackbar.make(findViewById(R.id.mainCourseCatalogContainer), R.string.network_error, Snackbar.LENGTH_SHORT).show();

                }

                break;


        }
    }

    private void initUi() {

        preferences = new MySharedPreferences(this);

        searchAcademicModelsList = SearchAcademicViewModel.getInstance().getCourseCatalogSearchModels();

        //regionRadioButton
        mainRadioGroup.check(R.id.allRadioBtn);
        selectedDegreeRG = getCheckedRadioBtn(mainRadioGroup);
        //endregion

        searchBtn.setOnClickListener(this);


    }

    private void getSpinnersData() {


        //regionAcademicSpinner

        for (int i = 0; i < AcademicCalenderViewModel.getInstance().getmAcadYearModels().size(); i++) {

            if (preferences.getData(Constants.LANGUAGE_CODE).equals(Constants.ENGLISH_CODE)) {

                acdYearsList.add(AcademicCalenderViewModel.getInstance().getmAcadYearModels().get(i).getACADYEARDESCREN());


            } else {

                acdYearsList.add(AcademicCalenderViewModel.getInstance().getmAcadYearModels().get(i).getACADYEARDESCRAR());


            }

        }


        spinnerAcadYearsAdapter = new ArrayAdapter<>(this, R.layout.spinner_background, acdYearsList);
        spinnerAcadYearsAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerAcadYear.setAdapter(spinnerAcadYearsAdapter);
        spinnerAcadYearsAdapter.notifyDataSetChanged();
        spinnerAcadYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                EdAcadYear = Integer.parseInt(AcademicCalenderViewModel.getInstance().getmAcadYearModels().get(position).getEDACADYEARID());
//                Log.i("Courseid", String.valueOf(EdAcadYear));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //endregion

        //regionCollageSpinner

        for (int j = 0; j < AcademicCalenderViewModel.getInstance().getmFaculties().size(); j++) {

            if (preferences.getData(Constants.LANGUAGE_CODE).equals(Constants.ENGLISH_CODE)) {

                CollageList.add(AcademicCalenderViewModel.getInstance().getmFaculties().get(j).getNameEn());


            } else {

                CollageList.add(AcademicCalenderViewModel.getInstance().getmFaculties().get(j).getNameAr());

            }

        }


        spinnerCollagesAdapter = new ArrayAdapter<>(this, R.layout.spinner_background, CollageList);
        spinnerCollagesAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerCollage.setAdapter(spinnerCollagesAdapter);
        spinnerCollagesAdapter.notifyDataSetChanged();
        spinnerCollage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                AsFacultyInfoId = AcademicCalenderViewModel.getInstance().getmFaculties().get(position).getAsFacultyInfoId();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //endregion

        //regionSemesterSpinner

        for (int k = 0; k < AcademicCalenderViewModel.getInstance().getmSemesterAcademicModel().size(); k++) {

            if (preferences.getData(Constants.LANGUAGE_CODE).equals(Constants.ENGLISH_CODE)) {

                SemesterList.add(AcademicCalenderViewModel.getInstance().getmSemesterAcademicModel().get(k).getSEMESTERDESCREN());


            } else {

                SemesterList.add(AcademicCalenderViewModel.getInstance().getmSemesterAcademicModel().get(k).getSEMESTERDESCRAR());

            }

        }


        spinnerSemestersAdapter = new ArrayAdapter<>(this, R.layout.spinner_background, SemesterList);
        spinnerSemestersAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerSemester.setAdapter(spinnerSemestersAdapter);
        spinnerSemestersAdapter.notifyDataSetChanged();
        spinnerSemester.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Semester = Integer.parseInt(AcademicCalenderViewModel.getInstance().getmSemesterAcademicModel().get(position).getEDSTUDSEMESTERID());
                Log.i("Courseid", String.valueOf(Semester));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //endregion

    }

    private void fetchFilterData() {

        searchAcademicModelsList = SearchAcademicViewModel.getInstance().getCourseCatalogSearchModels();

        adapter = new AcademicCalenderSearchAdapter(this, searchAcademicModelsList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adapter);

        //todo divider Color >>
//        recyclerView.addItemDecoration(new Utility.SimpleDividerItemDecoration(this));
    }

    public void showProgressDialog() {
        mProgressDialog = Utility.showProgressDialog(this, R.string.progressDialog_message);
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    private int getCheckedRadioBtn(RadioGroup radioGroup) {
        int checkedBtnId = radioGroup.getCheckedRadioButtonId();
        View radioBtn = radioGroup.findViewById(checkedBtnId);
        return radioGroup.indexOfChild(radioBtn);

    }

    @Override
    public void update(WebServices apiName) {


        if (apiName.equals(WebServices.AcademicCalender)) {

            getSpinnersData();
            hideProgressDialog();


        } else if (apiName.equals(WebServices.AcademicSearch)) {

            if (SearchAcademicViewModel.getInstance().getResult().equals("Ok")) {

                linearLayout.setVisibility(View.GONE);
                fetchFilterData();
                hideProgressDialog();
            } else {

//                finish();
//                linearLayout.setVisibility(View.VISIBLE);
                hideProgressDialog();
                Toast.makeText(this, R.string.error_data, Toast.LENGTH_SHORT).show();
            }


        }

    }

    @Override
    public void updateRefresh(WebServices method) {

    }

    @Override
    public void onBackPressed() {

        if (linearLayout.getVisibility() == View.GONE) {
            linearLayout.setVisibility(View.VISIBLE);
        } else {

            super.onBackPressed();

        }
    }

    @Override
    protected boolean isEnableHome() {
        return false;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return
                (super.onOptionsItemSelected(menuItem));

    }
}