package com.example.informatque.education.utils;

public class Constants {

    public static final String LANGUAGE_CODE = "LANGUAGE_CODE";
    public static final String ARABIC_CODE = "ar";
    public static final String ENGLISH_CODE = "en";

    //Tags
    public static final String LOGIN_TAG = "LOGIN_TAG";
    public static final String TRANSCRIPT_TAG = "TRANSCRIPT_TAG";
    public static final String COURSE_TAG = "COURSE_TAG";

    //URLs

    //old IP
//    public static final String IP = "http://192.168.1.111:8032/json/CRUDGenericHandler/";

    //new IP
    public static final String IP = "http://192.168.0.213/";

    //old URl School Record
//    public static final String TRANSCRIPT_URL = "MobED_CODE_SEMESTERCRUD.ashx?action=execPMOB_STUD_TRANSCRIPT";

    // NewURL

    //old url
//    public static final String Courses_URL = "UMISActions.ashx?action=execPMOB_STUD_SCHDL";

    //new url


    //POST Login
    public static final String USER_NAME = "UserName";
    public static final String PASSWORD = "Password";

    //POST TranscriptModel & Courses
    public static final String EDUCATION_STUDENT_ID_JSON = "EdStudId";
    public static final String FACULTY_INFORMATION_ID_JSON = "AsFacultyInfoId";
    public static final String EDUCATION_ACADEMIC_YEAR_JSON = "EdAcadYearId";
    public static final String EDUCATION_CODE_SEMESTER_JSON = "EdCodeSemesterId";

    //regionCourseTypes

    //URL
    public static final String TRANSCRIPT_URL = IP + "api/students/transcript/";
    public static final String LOGIN_URL = IP + "api/students/login/";
    public static final String DATA_URL_Courses = IP + "api/Faculties/CoursesCatalog/Filter/";
    public static final String SEARCH_COURSE_CATALOG = IP + "api/Faculties/CoursesCatalog/";
    public static final String SEARCH_ACADEMIC_CALENDAR = IP + "api/semester/semsterCalnd/";
    public static final String DATA_URL_AcdYears = IP + "api/Filter/FacultiesAcadYearsSemesters/";
    public static final String Courses_URL = IP + "api/students/schedule/";
    public static final String Schedule_URL = IP + "api/students/scheduledays/";


    //Tags used in the JSON String

    //JSON array name Courses
    public static final String COURSE_TYPES_ARRAY = "CourseTypes";
    public static final String FACULTIES_ARRAY = "Faculties";
    public static final String DEPARTMENTS_ARRAY = "Departments";
    public static final String COURSECATAOLG_ITEMS = "items";
    public static final String Academic_Calender_ITEMS = "items";

    public static final String JSON_ED_CODE_COURSE_ID = "ED_CODE_COURSE_ID";
    public static final String JSON_AsFacultyInfoId = "AsFacultyInfoId";
    public static final String JSON_DepId = "Id";


    //JSON array name AcademicYeras

    public static final String JSON_ARRAY_AcadYears = "AcadYears";
    public static final String JSON_ARRAY_Faculties = "Faculties";
    public static final String JSON_ARRAY_Semester = "SemesterModel";


    //Tags of Courses Spinners
    public static final String TAG_Name_Ar = "NameAr";
    public static final String TAG_Name_En = "NameEn";
    public static final String TAG_DESCR_AR = "DESCR_AR";
    public static final String TAG_DESCR_EN = "DESCR_EN";


    //Tags of Academic Spinners
    public static final String Acad_Year_Ar = "ACAD_YEAR_DESCR_AR";
    public static final String Acad_Year_En = "ACAD_YEAR_DESCR_EN";
    public static final String Semester_Ar = "SEMESTER_DESCR_AR";
    public static final String Semester_En = "SEMESTER_DESCR_EN";

    public static final String TAG_ED_CODE_COURSE_ID = "ED_CODE_COURSE_ID";


    //endregion

    //region Login Webservice data

    public static String et_UserName ;
    public static String et_Password ;


    public static final String EDUCATION_STUDENT_ID = "ED_STUD_ID";
    public static final String FACULTY_INFORMATION_ID = "AS_FACULTY_INFO_ID";
    public static final String EDUCATION_ACADEMIC_YEAR_ID = "ED_ACAD_YEAR_ID";
    public static final String EDUCATION_CODE_SEMESTER_ID = "ED_CODE_SEMESTER_ID";
    public static final String STUDENT_CODE = "STUD_FACULTY_CODE";
    public static final String STUDENT_NAME_ARABIC_ID = "FULL_NAME_AR";
    public static final String STUDENT_NAME_ENGLISH_ID = "FULL_NAME_EN";
    public static final String STUDENT_GENDER_ARABIC = "GENDER_DESCR_AR";
    public static final String STUDENT_GENDER_ENGLISH = "GENDER_DESCR_AR1";
    public static final String STUDENT_NATIONALITY_ARABIC = "NATION_DESCR_AR";
    public static final String STUDENT_NATIONALITY_ENGLISH = "NATION_DESCR_EN";
    public static final String IDENTITY_TYPE_ARABIC = "IDENT_AR";
    public static final String IDENTITY_TYPE_ENGLISH = "IDENT_EN";
    public static final String NATIONAL_ID = "NATIONAL_NUMBER";
    public static final String STUDENT_BIRTH_DATE = "BIRTH_DATE";
    public static final String STUDENT_EMAIL = "STUD_EMAIL";
    public static final String STUDENT_MOBILE_NUMBER = "STUD_MOBNO";
    public static final String FACULTY_NAME_ARABIC = "FACULTY_DESCR_AR";
    public static final String FACULTY_NAME_ENGLISH = "FACULTY_DESCR_EN";
    public static final String DEGREE_ARABIC = "DEGREE_AR";
    public static final String DEGREE_ENGLISH = "DEGREE_EN";
    public static final String MAJOR_ARABIC = "MAJOR_AR";
    public static final String MAJOR_ENGLISH = "MAJOR_EN";
    public static final String LEVEL_ARABIC = "LEVEL_AR";
    public static final String LEVEL_ENGLISH = "LEVEL_EN";
    public static final String SEMESTER_CREDIT_HOUR = "SEM_CH";
    public static final String FULL_FIELD_CREDIT_HOUR = "ACCUM_CH";
    public static final String CUMULATIVE_GPA = "ACCUM_GPA";
    public static final String SEMESTER_GPA = "SEM_GPA";
    public static final String ENROLLMENT_STATUS_ARABIC = "ENROLL_AR";
    public static final String ENROLLMENT_STATUS_ENGLISH = "ENROLL_EN";
    public static final String DEGREE_GRANTED = "GRADUATES_FLAG";

    //To be saved into shared preferences
//    public static final String STUDENT_NAME = "FULL_NAME";
    public static final String STUDENT_NAME_ENGLISH = "FULL_NAME_ENGLISH";
    public static final String STUDENT_NAME_ARABIC = "FULL_NAME_ARABIC";

    public static final String STUDENT_GENDER = "GENDER_DESCR";
    public static final String STUDENT_NATIONALITY = "NATION_DESCR";
    public static final String IDENTITY_TYPE = "IDENT";
    public static final String FACULTY_NAME = "FACULTY_DESCR";
    public static final String DEGREE = "DEGREE";
    public static final String MAJOR = "MAJOR";
    public static final String LEVEL = "LEVEL";
    public static final String ENROLLMENT_STATUS = "ENROLL";
    public static final String USERNAME_LOGIN = "USERNAME_LOGIN";
    //endregion

    //region TranscriptModel Webservice data

    //Academic Year
    public static final String EDUCATION_ACADEMIC_YEAR_ID_TRANSCRIPT = "ED_ACAD_YEAR_ID";
    public static final String ACADEMIC_YEAR_DESCRIPTION_ARABIC = "ACAD_YEAR_DESCR_AR";
    public static final String ACADEMIC_YEAR_DESCRIPTION_ENGLISH = "ACAD_YEAR_DESCR_EN";

    //SemesterModel
    public static final String EDUCATION_STUDENT_SEMESTER_ID = "ED_STUD_SEMESTER_ID";
    public static final String SEMESTER_DESCRIPTION_ARABIC = "SEMESTER_DESCR_AR";
    public static final String SEMESTER_DESCRIPTION_ENGLISH = "SEMESTER_DESCR_EN";

    //TranscriptModel
    public static final String COURSE_CODE = "COURSE_CODE";
    public static final String COURSE_DESCRIPTION_ARABIC = "COURSE_DESCR_AR";
    public static final String COURSE_DESCRIPTION_ENGLISH = "COURSE_DESCR_EN";
    public static final String COURSE_CREDIT_HOUR = "CREDIT_HOURS";
    public static final String COURSE_GRADE = "GRADING_AR";
    public static final String COURSE_STATUS = "Symbol_AR";

    public static final String ENROLLMENT_STATUS_ARABIC_TRANSCRIPT = "ENROLL_AR";
    public static final String ENROLLMENT_STATUS_ENGLISH_TRANSCRIPT = "ENROLL_EN";
    public static final String SEMESTER_GPA_TRANSCRIPT = "SEM_GPA";
    public static final String CUMULATIVE_GPA_TRANSCRIPT = "ACCUM_GPA";
    public static final String SEMESTER_TOTAL_CREDIT_HOUR = "SEM_CH";
    public static final String CUMULATIVE_CREDIT_HOUR = "ACCUM_CH";
    public static final String SEMESTER_POINTS = "SEM_POINT";
    public static final String CUMULATIVE_POINTS = "ACCUM_POINT";
    public static final String MAJOR_GPA = "MAJOR_CGPA";
    //endregion

    //region Courses Webservice data
    public static final String COURSE_STATUS_COURSES = "SYMBOL";
    public static final String FACULTIES = "Faculties";
    public static final String COURSES = "Courses";
    //endregion


    //region Schedule Calender

    public static final String BUILDING_DESCR_EN = "BUILDING_DESCR_EN";


    //endregion

    public static int FlaG_STUDENT_DATA;
    public static int FlaG_SETTING;
//    public static final int FlaG_STUDENT_DATA = 3;


}
