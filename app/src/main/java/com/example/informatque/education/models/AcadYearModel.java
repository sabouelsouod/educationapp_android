
package com.example.informatque.education.models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class AcadYearModel {

    @SerializedName("ACAD_YEAR_DESCR_AR")
    private String mACADYEARDESCRAR;
    @SerializedName("ACAD_YEAR_DESCR_EN")
    private String mACADYEARDESCREN;
    @SerializedName("ED_ACAD_YEAR_ID")
    private String mEDACADYEARID;
    @SerializedName("Semesters")
    private Object mSemesters;

    public String getACADYEARDESCRAR() {
        return mACADYEARDESCRAR;
    }

    public void setACADYEARDESCRAR(String ACADYEARDESCRAR) {
        mACADYEARDESCRAR = ACADYEARDESCRAR;
    }

    public String getACADYEARDESCREN() {
        return mACADYEARDESCREN;
    }

    public void setACADYEARDESCREN(String ACADYEARDESCREN) {
        mACADYEARDESCREN = ACADYEARDESCREN;
    }

    public String getEDACADYEARID() {
        return mEDACADYEARID;
    }

    public void setEDACADYEARID(String EDACADYEARID) {
        mEDACADYEARID = EDACADYEARID;
    }

    public Object getSemesters() {
        return mSemesters;
    }

    public void setSemesters(Object Semesters) {
        mSemesters = Semesters;
    }

}
