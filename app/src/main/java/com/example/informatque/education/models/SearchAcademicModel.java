
package com.example.informatque.education.models;

import javax.annotation.Generated;

import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class SearchAcademicModel {

    @SerializedName("ACTV_AR")
    private String mACTVAR;
    @SerializedName("ACTV_EN")
    private String mACTVEN;
    @SerializedName("ED_ACAD_YEAR_ID")
    private int mEDACADYEARID;
    @SerializedName("ED_CODE_SEMESTER_ID")
    private int mEDCODESEMESTERID;
    @SerializedName("ENT_DESCR_AR")
    private String mENTDESCRAR;
    @SerializedName("ENT_DESCR_EN")
    private String mENTDESCREN;
    @SerializedName("FROM_DATE")
    private String mFROMDATE;
    @SerializedName("ONLINE_FLG")
    private Boolean mONLINEFLG;
    @SerializedName("SEM_AR")
    private String mSEMAR;
    @SerializedName("SEM_EN")
    private String mSEMEN;
    @SerializedName("TO_DATE")
    private String mTODATE;
    @SerializedName("YEAR_CALENDER")
    private int mYEARCALENDER;

    public String getACTVAR() {
        return mACTVAR;
    }

    public void setACTVAR(String ACTVAR) {
        mACTVAR = ACTVAR;
    }

    public String getACTVEN() {
        return mACTVEN;
    }

    public void setACTVEN(String ACTVEN) {
        mACTVEN = ACTVEN;
    }

    public int getEDACADYEARID() {
        return mEDACADYEARID;
    }

    public void setEDACADYEARID(int EDACADYEARID) {
        mEDACADYEARID = EDACADYEARID;
    }

    public int getEDCODESEMESTERID() {
        return mEDCODESEMESTERID;
    }

    public void setEDCODESEMESTERID(int EDCODESEMESTERID) {
        mEDCODESEMESTERID = EDCODESEMESTERID;
    }

    public String getENTDESCRAR() {
        return mENTDESCRAR;
    }

    public void setENTDESCRAR(String ENTDESCRAR) {
        mENTDESCRAR = ENTDESCRAR;
    }

    public String getENTDESCREN() {
        return mENTDESCREN;
    }

    public void setENTDESCREN(String ENTDESCREN) {
        mENTDESCREN = ENTDESCREN;
    }

    public String getFROMDATE() {

        String[] tokens = mFROMDATE.split("T");

        mFROMDATE = tokens[0];

        return mFROMDATE;
    }

    public void setFROMDATE(String FROMDATE) {
        mFROMDATE = FROMDATE;
    }

    public Boolean getONLINEFLG() {
        return mONLINEFLG;
    }

    public void setONLINEFLG(Boolean ONLINEFLG) {
        mONLINEFLG = ONLINEFLG;
    }

    public String getSEMAR() {
        return mSEMAR;
    }

    public void setSEMAR(String SEMAR) {
        mSEMAR = SEMAR;
    }

    public String getSEMEN() {
        return mSEMEN;
    }

    public void setSEMEN(String SEMEN) {
        mSEMEN = SEMEN;
    }

    public String getTODATE() {

        String[] tokens = mTODATE.split("T");

        mTODATE = tokens[0];

        return mTODATE;
    }

    public void setTODATE(String TODATE) {
        mTODATE = TODATE;
    }

    public int getYEARCALENDER() {
        return mYEARCALENDER;
    }

    public void setYEARCALENDER(int YEARCALENDER) {
        mYEARCALENDER = YEARCALENDER;
    }

}
