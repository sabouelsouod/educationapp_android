package com.example.informatque.education.viewModels;

import android.content.Context;
import android.util.Log;

import com.example.informatque.education.EducationAppController;
import com.example.informatque.education.helpers.Updatable;
import com.example.informatque.education.helpers.retrofit.EducationServices;
import com.example.informatque.education.helpers.retrofit.WebServices;
import com.example.informatque.education.models.AcademicYearModel;
import com.example.informatque.education.utils.Utils;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SchoolRecordViewModel {

    private static final String TAG = SchoolRecordViewModel.class.getSimpleName();

    private Updatable updatable;
    public static SchoolRecordViewModel instance = new SchoolRecordViewModel();


    @SerializedName(value = "result")
    String result;

    @SerializedName(value = "details")
    String details;

    @SerializedName(value = "items")
    ArrayList<AcademicYearModel> academicYearModels;

    public Context context;


    public ArrayList<AcademicYearModel> getAcademicYearModels() {
        return academicYearModels;
    }

    public String getResult() {
        return result;
    }

    public String getDetails() {
        return details;
    }


    public static SchoolRecordViewModel getInstance() {
        if (instance == null)
            instance = new SchoolRecordViewModel();
        return instance;
    }

    /*---------------------------------------------------*/
    public void getDAta(String StudId, String FacultyInfo, String EdYearId, String EdCodeSemester, Context context, final Updatable updatable) {
        this.updatable = updatable;
        this.context = context;


        EducationAppController application = EducationAppController.get(context);
        EducationServices educationServices = application.getJMTServices();


        JsonObject obj = new JsonObject();
        obj.addProperty("EdStudId", StudId);
        obj.addProperty("AsFacultyInfoId", FacultyInfo);
        obj.addProperty("EdAcadYearId", EdYearId);
        obj.addProperty("EdCodeSemesterId", EdCodeSemester);


        Log.i("Boady", obj.toString());


        Call<SchoolRecordViewModel> call = educationServices.TranscriptData(obj);
        call.enqueue(new Callback<SchoolRecordViewModel>() {
            @Override
            public void onResponse(Call<SchoolRecordViewModel> call, Response<SchoolRecordViewModel> response) {
                onSuccess(response);
            }

            @Override
            public void onFailure(Call<SchoolRecordViewModel> call, Throwable t) {
                onError();
                Log.e(TAG, "onFailure");
                Log.e(TAG, "Throwable" + t.getMessage());
            }

        });


    }


    /*---------------------------------------------------*/
    private void onSuccess(Response<SchoolRecordViewModel> response) {
        // TODO: 11/15/2017 Mapping Response Data To View Model Singleton object
        instance = response.body();
        if (instance != null) {
            updatable.update(WebServices.SchoolRecords);
        }
    }

    private void onError() {
        Utils.getInstance(context).dismissProgress();
    }

}
