package com.example.informatque.education.models;

/**
 * Created by ibrashraf on 9/27/2017.
 */

public class DataModel {


    public int icon;
    public String name ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    // Constructor.
    public DataModel(String name , int icon ) {

        this.icon = icon;
        this.name = name;
    }
}
