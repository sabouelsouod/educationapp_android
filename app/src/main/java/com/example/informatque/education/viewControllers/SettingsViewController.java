package com.example.informatque.education.viewControllers;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.informatque.education.R;
import com.example.informatque.education.helpers.ParentActivity;
import com.example.informatque.education.utils.Constants;
import com.example.informatque.education.utils.MySharedPreferences;
import com.example.informatque.education.utils.Utility;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SettingsViewController extends ParentActivity {

    @BindView(R.id.languageChooserSpinner)
    Spinner languageChooserSpinner;
    MySharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        preferences = new MySharedPreferences(this);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.setting_language, R.layout.spinner_background);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        languageChooserSpinner.setAdapter(adapter);
        languageChooserSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Locale locale = Utility.getCurrentLocale(SettingsViewController.this);
                Log.i("current locale", locale.getLanguage());
                String lang = locale.getLanguage();
                if (position == 1) {

                    if (lang.equals("en")) {
                        Utility.setLocale(getBaseContext(), Constants.ARABIC_CODE);
                        startActivity(new Intent(SettingsViewController.this, HomeViewComtroller.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        preferences.addData(Constants.LANGUAGE_CODE, Constants.ARABIC_CODE);


                    } else {
                        Utility.setLocale(getBaseContext(), Constants.ENGLISH_CODE);
                        startActivity(new Intent(SettingsViewController.this, HomeViewComtroller.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        preferences.addData(Constants.LANGUAGE_CODE, Constants.ENGLISH_CODE);

                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_settings;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }


    @Override
    protected boolean isEnableTitle() {
        return false;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected int getTitleResource() {
        return R.string.textView_settings_title;
    }

    @Override
    public void onClick(View v) {

    }


    @Override
    protected boolean isEnableHome() {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, HomeViewComtroller.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
