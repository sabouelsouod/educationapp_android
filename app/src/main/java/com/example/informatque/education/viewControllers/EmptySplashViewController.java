package com.example.informatque.education.viewControllers;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.example.informatque.education.R;
import com.example.informatque.education.utils.MySharedPreferences;


public class EmptySplashViewController extends AppCompatActivity {


    MySharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empty);

        preferences = new MySharedPreferences(this);

        if (preferences.getBoolean("logged")) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(getBaseContext(), HomeViewComtroller.class));
                    finish();
                }
            }, 1000);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    startActivity(new Intent(getBaseContext(), SplashViewController.class));
                    finish();
                }
            }, 500);
        }


    }
}
