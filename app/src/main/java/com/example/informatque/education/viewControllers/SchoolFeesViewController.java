package com.example.informatque.education.viewControllers;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.informatque.education.R;
import com.example.informatque.education.helpers.ParentActivity;
import com.example.informatque.education.utils.Constants;
import com.example.informatque.education.utils.MySharedPreferences;
import com.example.informatque.education.utils.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by ibrashraf on 10/4/2017.
 */

public class SchoolFeesViewController extends ParentActivity {

    //regionBindViews

    @BindView(R.id.studyFeesDataCard)
    CardView studyFeesDataCard;

    @BindView(R.id.btn_studyFees)
    Button btn_studuyFeesData;

    @BindView(R.id.anotherFeesDataCard)
    CardView anotherFeesDataCard;

    @BindView(R.id.btn_anotherFees)
    Button btnAnotherFeesData;

    @BindView(R.id.btn_Receipts)
    Button btnReceipts;

    @BindView(R.id.recycleFees)
    RecyclerView recyclerView;

    @BindView(R.id.mainFeesViewContainer)
    LinearLayout mainFeesLayoutContainer;


    //endregion

//    CourseCatalogAdapter adapter;
//    ArrayList<CourseCatalog> receiptsFees = new ArrayList<>();

    String languageCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);

        MySharedPreferences preferences = new MySharedPreferences(this);
        languageCode = preferences.getData(Constants.LANGUAGE_CODE);

        registerViews();

        btn_studuyFeesData.setOnClickListener(this);
        btnAnotherFeesData.setOnClickListener(this);
        btnReceipts.setOnClickListener(this);


    }


    void registerViews() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.addItemDecoration(new Utility.SimpleDividerItemDecoration(this));
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_fees;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean isEnableHome() {
        return false;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected int getTitleResource() {
        return R.string.textView_shcoolFees_title;
    }

    @Override
    protected boolean isEnableTitle() {
        return false;
    }

    @Override
    public void onBackPressed() {

        if (recyclerView.getVisibility() == View.VISIBLE) {
            recyclerView.setVisibility(View.GONE);
            mainFeesLayoutContainer.setVisibility(View.VISIBLE);
        } else
            super.onBackPressed();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {


            case R.id.btn_studyFees:
                if (studyFeesDataCard.getVisibility() == View.VISIBLE) {
                    studyFeesDataCard.setVisibility(View.GONE);
                    Utility.settingArrowDrawable(btn_studuyFeesData, languageCode, activity, R.drawable.down);
                } else {
                    studyFeesDataCard.setVisibility(View.VISIBLE);
//                    YoYo.with(Techniques.FadeInUp).playOn(studentAcademicDataCard);
                    Utility.settingArrowDrawable(btn_studuyFeesData, languageCode, activity, R.drawable.up);
                }
                break;

            case R.id.btn_anotherFees:
                if (anotherFeesDataCard.getVisibility() == View.VISIBLE) {
                    anotherFeesDataCard.setVisibility(View.GONE);
                    Utility.settingArrowDrawable(btnAnotherFeesData, languageCode, activity, R.drawable.down);
                } else {
                    anotherFeesDataCard.setVisibility(View.VISIBLE);
//                    YoYo.with(Techniques.FadeInUp).playOn(studentAcademicDataCard);
                    Utility.settingArrowDrawable(btnAnotherFeesData, languageCode, activity, R.drawable.up);
                }
                break;


            case R.id.btn_Receipts:
                if (Utility.isNetworkAvailable(this)) {

//                    receiptsFees.clear();
                    mainFeesLayoutContainer.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                } else {
                    Snackbar.make(findViewById(R.id.mainCourseCatalogContainer), R.string.network_error, Snackbar.LENGTH_SHORT).show();

                }

                break;


        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:

                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));

    }

}
