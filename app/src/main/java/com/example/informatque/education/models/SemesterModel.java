
package com.example.informatque.education.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class SemesterModel {

    @SerializedName("ED_STUD_SEMESTER_ID")
    private String mEDSTUDSEMESTERID;
    @SerializedName("SEMESTER_DESCR_AR")
    private String mSEMESTERDESCRAR;
    @SerializedName("SEMESTER_DESCR_EN")
    private String mSEMESTERDESCREN;
    @SerializedName("Transcripts")
    private List<TranscriptModel> mTranscriptModels;

    public String getEDSTUDSEMESTERID() {
        return mEDSTUDSEMESTERID;
    }

    public void setEDSTUDSEMESTERID(String EDSTUDSEMESTERID) {
        mEDSTUDSEMESTERID = EDSTUDSEMESTERID;
    }

    public String getSEMESTERDESCRAR() {
        return mSEMESTERDESCRAR;
    }

    public void setSEMESTERDESCRAR(String SEMESTERDESCRAR) {
        mSEMESTERDESCRAR = SEMESTERDESCRAR;
    }

    public String getSEMESTERDESCREN() {
        return mSEMESTERDESCREN;
    }

    public void setSEMESTERDESCREN(String SEMESTERDESCREN) {
        mSEMESTERDESCREN = SEMESTERDESCREN;
    }

    public List<TranscriptModel> getTranscripts() {
        return mTranscriptModels;
    }

    public void setTranscripts(List<TranscriptModel> transcriptModels) {
        mTranscriptModels = transcriptModels;
    }

}
