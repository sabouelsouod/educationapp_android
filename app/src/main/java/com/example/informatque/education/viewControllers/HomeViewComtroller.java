package com.example.informatque.education.viewControllers;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.informatque.education.Adapters.DrawerItemCustomAdapter;
import com.example.informatque.education.R;
import com.example.informatque.education.helpers.ParentActivity;
import com.example.informatque.education.models.DataModel;
import com.example.informatque.education.utils.Constants;
import com.example.informatque.education.utils.MySharedPreferences;
import com.example.informatque.education.utils.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;


public class HomeViewComtroller extends ParentActivity {

    //regionBindViews
    @BindView(R.id.cv_StudentData)
    CardView cv_StudentData;

    @BindView(R.id.cv_SchoolRecord)
    CardView cv_SchoolRecord;

    @BindView(R.id.cv_CoursesRegistration)
    CardView cv_CoursesRegistration;

    @BindView(R.id.cv_Settings)
    CardView cv_Settings;

    @BindView(R.id.cv_CourseCalendar)
    CardView cv_AcademicCalendar;

    @BindView(R.id.cv_courseCatalog)
    CardView cv_CourseCatalog;

    @BindView(R.id.cv_schoolShedual)
    CardView cv_SchoolShedual;

    @BindView(R.id.cv_Fees)
    CardView cv_Fees;

    //endregion

    MySharedPreferences preferences;


    ImageView imageProfile;

    TextView tvProfile;
    LinearLayout Header;

    private String[] mNavigationDrawerItemTitles;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    Toolbar toolbar;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;

    RelativeLayout layoutParentRating;

    android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;

    @Override
    public void onBackPressed() {

        if (preferences.getBoolean("rememberMe")) {

            super.onBackPressed();

        } else {

            preferences.addData("logged", false);
            super.onBackPressed();


        }
    }

    @Override
    protected void onStart() {

        if (preferences.getBoolean("logged")) {

            String language = preferences.getData(Constants.LANGUAGE_CODE);


            imageProfile.setImageResource(R.drawable.ic_profile);

            if (language.equals(Constants.ENGLISH_CODE)) {

                tvProfile.setText(preferences.getData(Constants.STUDENT_NAME_ENGLISH));


            } else if (language.equals(Constants.ARABIC_CODE)) {


                tvProfile.setText(preferences.getData(Constants.STUDENT_NAME_ARABIC));

            }


        } else {

            tvProfile.setText("");


        }

        super.onStart();

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        preferences = new MySharedPreferences(this);


        layoutParentRating = findViewById(R.id.layout_parent_rating);


        mNavigationDrawerItemTitles = getResources().getStringArray(R.array.navigation_drawer_items_array);

        Header = findViewById(R.id.header_drawer);

        mDrawerLayout = findViewById(R.id.drawer_layout);

        imageProfile = findViewById(R.id.image_profile);

        tvProfile = findViewById(R.id.tv_profile);

        mDrawerList = findViewById(R.id.left_drawer);

        String language = preferences.getData(Constants.LANGUAGE_CODE);

        DataModel[] drawerItem = new DataModel[3];


        if (language.equals(Constants.ARABIC_CODE)) {


            drawerItem[0] = new DataModel("بيانات الطالب", R.drawable.student_data);
            drawerItem[1] = new DataModel("الإعدادات", R.drawable.settings);
            drawerItem[2] = new DataModel("تسجيل الخروج", R.drawable.ic_power_settings_new_black_24dp);

            DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter(this, R.layout.list_view_item_navigation, drawerItem);

            mDrawerList.setAdapter(adapter);


        } else {


            drawerItem[0] = new DataModel("Student Data", R.drawable.student_data);
            drawerItem[1] = new DataModel("Setting", R.drawable.settings);
            drawerItem[2] = new DataModel("LogOut", R.drawable.ic_power_settings_new_black_24dp);

            DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter(this, R.layout.list_view_item_navigation, drawerItem);

            mDrawerList.setAdapter(adapter);

        }


        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());


        final LinearLayout btnMenu = findViewById(R.id.btn_menu);


        btnMenu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (mDrawerLayout.isDrawerOpen(Header)) {
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                    // btnMenu.setImageResource(R.drawable.ic_menu);
                } else {
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                    // btnMenu.setImageResource(R.drawable.ic_menu_select);
                }
            }
        });


        cv_StudentData.setOnClickListener(this);
        cv_SchoolRecord.setOnClickListener(this);
        cv_CoursesRegistration.setOnClickListener(this);
        cv_Settings.setOnClickListener(this);
        cv_AcademicCalendar.setOnClickListener(this);
        cv_CourseCatalog.setOnClickListener(this);
        cv_SchoolShedual.setOnClickListener(this);
        cv_Fees.setOnClickListener(this);


    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }

    }

    @Override
    protected int getLayoutResource() {
        return (R.layout.home);

    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean isEnableHome() {
        return true;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected int getTitleResource() {
        return R.string.textView_text_home_title;
    }

    @Override
    protected boolean isEnableTitle() {
        return false;
    }

    private void selectItem(int position) {
        String language = preferences.getData(Constants.LANGUAGE_CODE);

        Intent i;
        Bundle b = new Bundle();


        Intent intent = null;


        switch (position) {
            case 0:

                if (preferences.getBoolean("logged")) {

                    if (Utility.isNetworkAvailable(this)) {
                        i = new Intent(getBaseContext(), StudetDataViewController.class);
                        startActivity(i);
                    } else {
                        Snackbar.make(findViewById(R.id.activity_home), R.string.network_error, Snackbar.LENGTH_SHORT).show();
                    }

                } else {

                    preferences.addData("logged", true);
                    i = new Intent(getBaseContext(), LoginViewController.class);
                    i.putExtra("Flag", "studentdata");
                    startActivity(i);
                }
                break;


            case 1:

                intent = new Intent(HomeViewComtroller.this, SettingsViewController.class);
                startActivity(intent);
                break;

            case 2:
//                mDrawerLayout.closeDrawer(Gravity.LEFT);
                preferences.addData("logged", false);
                if (language.equals(Constants.ENGLISH_CODE)) {

//                    logOut();
//                    layoutParentRating.setVisibility(View.VISIBLE);
                    imageProfile.setImageResource(R.drawable.ic_logo_2);

                    tvProfile.setText(null);

                    Toast.makeText(this, "LogOut Sucsess", Toast.LENGTH_SHORT).show();

                } else if (language.equals(Constants.ARABIC_CODE)) {

//                    logOut();
//                    layoutParentRating.setVisibility(View.VISIBLE);

                    imageProfile.setImageResource(R.drawable.ic_logo_2);

                    tvProfile.setText(null);

                    Toast.makeText(this, "تم تسجيل الخروج بنجاح", Toast.LENGTH_SHORT).show();

                }
                break;


            default:
                ;


        }


        mDrawerList.setItemChecked(position, true);
        mDrawerList.setSelection(position);
        mDrawerLayout.closeDrawer(Header);


    }

    public void logOut() {


        new AlertDialog.Builder(HomeViewComtroller.this)

                .setMessage(getResources().getString(R.string.logout_msg))
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();


    }

    @Override
    public void onClick(View v) {
        Intent i;
        Bundle b = new Bundle();

        switch (v.getId()) {


            case R.id.cv_StudentData:


                if (Utility.isNetworkAvailable(this)) {

                    if (preferences.getBoolean("logged")) {

                        i = new Intent(getBaseContext(), StudetDataViewController.class);
                        startActivity(i);

                    } else {

                        i = new Intent(getBaseContext(), LoginViewController.class);
                        i.putExtra("Flag", "studentdata");
                        startActivity(i);
                    }

                } else {

                    Snackbar.make(findViewById(R.id.activity_home), R.string.network_error, Snackbar.LENGTH_SHORT).show();

                }
                break;

            case R.id.cv_Settings:

                startActivity(new Intent(getBaseContext(), SettingsViewController.class));

                break;


            case R.id.cv_SchoolRecord:

                if (preferences.getBoolean("logged")) {

                    if (Utility.isNetworkAvailable(this)) {
                        i = new Intent(getBaseContext(), SchoolRecordViewController.class);
                        startActivity(i);
                    } else {
                        Snackbar.make(findViewById(R.id.activity_home), R.string.network_error, Snackbar.LENGTH_SHORT).show();
                    }


                } else {
                    i = new Intent(getBaseContext(), LoginViewController.class);
                    i.putExtra("Flag", "schoolrecord");
                    startActivity(i);

                }

                break;


            case R.id.cv_CoursesRegistration:

                if (preferences.getBoolean("logged")) {

                    if (Utility.isNetworkAvailable(this)) {

                        i = new Intent(getBaseContext(), CoursesRegistrationViewController.class);
                        startActivity(i);
                    } else {
                        Snackbar.make(findViewById(R.id.activity_home), R.string.network_error, Snackbar.LENGTH_SHORT).show();
                    }


                } else {
                    i = new Intent(getBaseContext(), LoginViewController.class);
                    i.putExtra("Flag", "course");
                    startActivity(i);
                }


                break;

            case R.id.cv_courseCatalog:

                if (Utility.isNetworkAvailable(this)) {
                    startActivity(new Intent(getBaseContext(), CourseCatalogViewController.class));
                } else {
                    Snackbar.make(findViewById(R.id.activity_home), R.string.network_error, Snackbar.LENGTH_SHORT).show();

                }
                break;


            case R.id.cv_CourseCalendar:
                if (Utility.isNetworkAvailable(this)) {
                    startActivity(new Intent(getBaseContext(), AcademicCalendarViewController.class));
                } else {
                    Snackbar.make(findViewById(R.id.activity_home), R.string.network_error, Snackbar.LENGTH_SHORT).show();
                }

                break;

            case R.id.cv_Fees:


                if (Utility.isNetworkAvailable(this)) {

                    startActivity(new Intent(getBaseContext(), SchoolFeesViewController.class));
                } else {
                    Snackbar.make(findViewById(R.id.activity_home), R.string.network_error, Snackbar.LENGTH_SHORT).show();
                }


                break;

            case R.id.cv_schoolShedual:


                if (preferences.getBoolean("logged")) {

                    if (Utility.isNetworkAvailable(this)) {


                        i = new Intent(getBaseContext(), SchoolScheduleViewController.class);
                        startActivity(i);
                    } else {
                        Snackbar.make(findViewById(R.id.activity_home), R.string.network_error, Snackbar.LENGTH_SHORT).show();
                    }


                } else {
                    preferences.addData("logged", true);
                    i = new Intent(getBaseContext(), LoginViewController.class);
                    i.putExtra("Flag", "Schedule");
                    startActivity(i);
                }

                break;


        }

    }

}
