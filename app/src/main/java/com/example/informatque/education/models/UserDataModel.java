package com.example.informatque.education.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class UserDataModel implements Serializable  {

    @SerializedName(value = "ED_STUD_ID")
    String studentID;

    @SerializedName(value = "AS_FACULTY_INFO_ID")
    String facultyInfo;

    @SerializedName(value = "ED_CODE_SEMESTER_ID")
    String semesterCode;


    @SerializedName(value = "ED_ACAD_YEAR_ID")
    String academicYear;

    @SerializedName(value = "YEAR_DESC_EN")
    String yearDescrip_EN;

    @SerializedName(value = "YEAR_DESC_AR")
    String yearDescrip_AR;

    @SerializedName(value = "YEAR_CODE")
    String yearCode;

    @SerializedName(value = "SEM_DESC_EN")
    String semsDescrip_EN;


    @SerializedName(value = "SEM_DESSEM_DESC_ARC_EN")
    String semsDescrip_AR;

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public String getStudentID() {
        return studentID;
    }

    public String getFacultyInfo() {
        return facultyInfo;
    }

    public String getSemesterCode() {
        return semesterCode;
    }

//    public String getAcademicYears() {
//        return academicYear;
//    }

    public String getYearDescrip_EN() {
        return yearDescrip_EN;
    }

    public String getYearDescrip_AR() {
        return yearDescrip_AR;
    }

    public String getYearCode() {
        return yearCode;
    }

    public String getSemsDescrip_EN() {
        return semsDescrip_EN;
    }

    public String getSemsDescrip_AR() {
        return semsDescrip_AR;
    }
}
