package com.example.informatque.education.viewControllers;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.informatque.education.R;
import com.example.informatque.education.helpers.ParentActivity;
import com.example.informatque.education.utils.Constants;
import com.example.informatque.education.utils.MySharedPreferences;
import com.example.informatque.education.utils.Utility;

public class SplashViewController extends ParentActivity {

    MySharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = new MySharedPreferences(this);


        if (preferences.getBoolean("firstTimeLog")) {

            String lang = preferences.getData(Constants.LANGUAGE_CODE);
            if (lang.equalsIgnoreCase("ar"))
                Utility.setLocale(getBaseContext(), Constants.ARABIC_CODE);

            else {
                Utility.setLocale(getBaseContext(), Constants.ENGLISH_CODE);
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(getBaseContext(), HomeViewComtroller.class));
                    finish();
                }
            }, 1600);
        } else {
            preferences.addData("firstTimeLog", true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    startActivity(new Intent(getBaseContext(), LanguageViewController.class));
                    finish();
                }
            }, 1600);
        }


    }

    @Override
    protected int getLayoutResource() {

        return R.layout.activity_splash_view_controller;

    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected int getTitleResource() {
        return 0;
    }

    @Override
    protected boolean isEnableTitle() {
        return false;
    }

    @Override
    protected boolean isEnableHome() {
        return false;
    }


    @Override
    public void onClick(View v) {

    }
}
